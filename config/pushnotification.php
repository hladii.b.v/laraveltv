<?php
/**
 * @see https://github.com/Edujugon/PushNotification
 */

return [
    'gcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'My_ApiKey',
    ],
    'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAIKLIisI:APA91bEjlLgOqPvR1oc4fAoR_Xq8JDD_aRrzWn6P5Q2ZUKBXZ92Qo0itxmn1LFW_OzpPhu0Op0UnFEebkcNQz1SKS70j4j7heeuK5sgH8biH-kumDSmLRPNqBgqrAjn3N-RGlEk9lOv2',
    ],
    'apn' => [
        'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
        'passPhrase' => 'secret', //Optional
        'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
        'dry_run' => true,
    ],
];
