<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Artist extends Model
{

    use SoftDeletes;        

    protected $appends = array('logoSrc','coverSrc');

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'videos_artist');
    }

    public function apiDataArr(){

        $item = [
            "id" => $this->id,
            "title" => $this->artistname,
            "subtitle" => "",
            "thumbnail" => $this->logoSrc,
            "cover" => $this->coverSrc,
            "type" => "artist"
        ];
        return $item;
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'category_artist');
    }


    public function getlogoSrcAttribute(){
        return $this->logo ? asset('storage/'.$this->logo) : asset('images/placeholder.png');
    }

    public function getCoverSrcAttribute(){
        return $this->coverImage ? asset('storage/'.$this->coverImage) : asset('images/placeholder.png');
    }
}
