<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    use SoftDeletes;   

    public function videos()
    {
        return $this->belongsToMany(Video::class, 'video_category');
    }

    public function apiDataArr(){
        $item = [
            "id" => $this->id,
            "title" => $this->title,
            "subtitle" => "",
            "thumbnail" => $this->imageSrc,
            "cover" => $this->coverSrc,
            "type" => "category"
        ];
        return $item;
    }

    public function artists()
    {
        return $this->belongsToMany(Artist::class, 'category_artist');
    }

    public function parentId(){
        return $this->belongsTo(Category::class);
    }

    public function children()
    {
        return Category::where('parent_id', $this->id)->orderBy('priority','DESC')->get();
    }


    public function getImageSrcAttribute(){

        return $this->thumbnail ? asset('storage/'.$this->thumbnail) : asset('images/placeholder.png');
    }

    public function getCoverSrcAttribute(){
        return $this->coverImage ? asset('storage/'.$this->coverImage) : asset('images/placeholder.png');
    }
}
