<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ios' => [
                'min' => $this['ios_app_min_version'] ?? '',
                'current' => $this['ios_app_current_version'] ?? '',
            ],
            'android' => [
                'min' => $this['android_app_min_version'] ?? '',
                'current' => $this['android_app_current_version'] ?? '',
            ],
        ];
    }
}
