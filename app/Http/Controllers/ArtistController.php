<?php

namespace App\Http\Controllers;

use App\Artist;
use App\Category;
use App\Video;
use Illuminate\Http\Request;

class ArtistController extends Controller
{
    public function index( Artist $artist )
    {

        $categories = Category::whereHas('artists', function ($query) use($artist) {
            $query->where('artist_id',  $artist->id);
        })->orderByDesc('priority')->get();
        $videos = Video::whereHas('artists', function ($query) use($artist) {
            $query->where('artist_id',  $artist->id);
        })->get();
        return view('artist')->withArtist($artist)->withCtegories($categories)->withVideos($videos);
    }
    public function all_artists ()
    {

        $artists = Artist::orderByDesc('featured','priority')->get();

        return view('artists')->withArtists($artists);
    }
}
