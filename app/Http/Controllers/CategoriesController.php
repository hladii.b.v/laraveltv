<?php

namespace App\Http\Controllers;

use App\Category;
use App\Artist;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{
    public function index(Request $request, Category $category )
    {
        if($category->id==Null){
            $category = Category::orderByDesc('priority')->first();
        }
        $categories = Category::orderByDesc('priority')->get();

        $artists = Artist::whereHas('categories', function ($query) use($category) {
            $query->where('category_id',  $category->id);
        })->get();
        $videos = Video::whereHas('categories', function ($query) use($category) {
            $query->where('category_id',  $category->id);
        })->get();

        return view('categories')->withCtegory( $category->id)->withCtegories($categories)->withVideos($videos)->withArtists($artists);

    }
}
