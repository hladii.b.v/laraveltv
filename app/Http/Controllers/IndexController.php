<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class IndexController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function getApp()
    {
        return view('pages.download');
    }


    public function contact()
    {
        return view('pages.contact');
    }

    public function ContactStore(Request $request)
    {
        $request->validate([
            'phone' => 'required|unique:subscriptions,phone',
        ]);

        $c = new Contact();
        $c->name = $request->name;
        $c->email = $request->email;
        $c->phone = $request->phone;
        $c->subject = $request->subject;
        $c->message = $request->message;
        $c->save();
        return '{ "alert": "success", "message": "תודה! פנייתך התקבלה בהצלחה" }';


    }


}
