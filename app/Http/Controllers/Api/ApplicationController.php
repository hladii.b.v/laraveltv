<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ApplicationResource;
use Illuminate\Http\Request;

class ApplicationController extends Controller
{
    public function version(){
        $data = [
            'ios_app_current_version'=> setting('application.ios_app_current_version'),
            'ios_app_min_version'=> setting('application.ios_app_min_version'),
            'android_app_current_version'=> setting('application.android_app_current_version'),
            'android_app_min_version'=> setting('application.android_app_min_version'),
            ];
        return new ApplicationResource($data);
    }
}
