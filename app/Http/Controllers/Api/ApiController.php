<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use App\Page;
use App\Video;
use App\Artist;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ApiController extends Controller
{
    public function home()
    {
        $json= [];
        $categories = Category::whereNull('parent_id')->orderByDesc('priority')->get();

        $artists = Artist::orderByDesc('featured','priority')->get();

        // artists

        $json['הכוכבים'] = [];
        foreach ($artists as $artist){

            $obj_child = [
                "id" => $artist->id,
                "title" => $artist->artistname,
                "priority" => $artist->priority,
                "thumbnail" => $artist->logoSrc,
                'coverImage' => $artist->coverSrc,
                'description' => $artist->description,
                "type" => "artist"
            ];
            $json['הכוכבים'][] =   $obj_child;

        }

        if ($categories) {

            foreach ($categories as $cat) {

                $videos = Video::whereHas('categories', function ($query) use($cat) {
                    $query->where('category_id',  $cat->id);
                })->orderByDesc('priority')->get();

                if ($videos) {
                    foreach ( $videos as $video ) {
                        $obj_videos = [
                            "id" => $video->id,
                            "title" => $video->title,
                            "hash" => $video->hash(),
                            "videoURL" => $video->videoURL,
                            "priority" => $video->priority,
                            "thumbnail" => $video->imageSrc,
                            'coverImage' => $video->coverSrc,
                            "type" => "video"
                        ];
                        $obj[] =$obj_videos;
                    }
                }

                $children = $cat->children();
                if ($children) {
                    foreach ( $children as $child ) {
                        $obj_child = [
                            "id" => $child->id,
                            "title" => $child->title,
                            "priority" => $child->priority,
                            "thumbnail" => $child->imageSrc,
                            'coverImage' => $child->coverSrc,
                            'description' => $child->description,
                            "type" => "category"
                        ];
                        $obj[] =   $obj_child;
                    }
                }

                if (isset($obj)){
                    $obj = array_values(Arr::sort($obj, function ($value) {
                        return $value['priority'];
                    }));
                    $json[$cat->title] = $obj;
                    $obj = [];
                }
            }
        }
        else {
            $json= array('status' =>false );
        }
        $json['home-video'] = ['title' => setting('site.homeTitle'), 'img'=> asset('storage/'.setting('site.homeImage')), 'description' => setting('site.homeDescription'),  'url'=>setting('site.homeVideoUrl')] ;
        return response($json);
    }

    public function category($category_id)
    {
        $json= [];
        $categories = Category::orderByDesc('priority')->where('id',  $category_id)->get();
        if ($categories) {
            foreach ($categories as $cat) {

                $videos = Video::whereHas('categories', function ($query) use($cat) {
                    $query->where('category_id',  $cat->id);
                })->orderByDesc('priority')->get();
                if ($videos) {
                    foreach ( $videos as $video ) {
                        $obj_videos = [
                            "id" => $video->id,
                            "title" => $video->title,
                            "hash" => $video->hash(),
                            "videoURL" => $video->videoURL,
                            "priority" => $video->priority,
                            "thumbnail" => $video->imageSrc,
                            'coverImage' => $video->coverSrc,
                            "type" => "video"
                        ];
                        $obj[] = $obj_videos;
                    }
                }

                $children = $cat->children();
                if ($children) {
                    foreach ( $children as $child ) {
                        $obj_child = [
                            "id" => $child->id,
                            "title" => $child->title,
                            "priority" => $child->priority,
                            "thumbnail" => $child->imageSrc,
                            'coverImage' => $child->coverSrc,
                            'description' => $child->description,
                            "type" => "category"
                        ];
                        $obj[] = $obj_child;
                    }
                }

                if (isset($obj)){
                    $obj = array_values(Arr::sort($obj, function ($value) {
                        return $value['priority'];
                    }));
                    $json[$cat->title] = $obj;
                    $obj = [];
                }
            }
        }
        else {
            $json['status'] = false;
        }
        return response($json);
    }

    public function video($video_id, $hash)
    {
        $json= [];
        $video = Video::where('id',  $video_id)->with(['artists', 'categories'])->first();
        if ($hash != $video->hash() ) die(  "wrong hash");
        if ($video) {
            $items = [];
            if (isset($video->categories)){
                foreach ( $video->categories as $child ) {
                    $obj_child = [
                        "id" => $child->id,
                        "title" => $child->title,
                        "priority" => $child->priority,
                        "thumbnail" => $child->imageSrc,
                        "type" => "category",
                        'coverImage' => $child->coverSrc,
                        'description' => $child->description,
                    ];
                    $array[] = $obj_child;
                }
                if (isset($array)){
                    $items['categories'] = $array;
                }
            }

            if (isset($video->artists)){
                $array_a = [];
                foreach ( $video->artists as $artist ) {
                    $obj_child = [
                        'id' => $artist->id,
                        'artistName' => $artist->artistName,
                        'logo' => $artist->logo ? asset('storage/'.$artist->logo) : asset('images/placeholder.png'),
                        'image1' => $artist->image1 ? asset('storage/'.$artist->image1) : asset('images/placeholder.png'),
                        'image2' => $artist->image2 ? asset('storage/'.$artist->image2) : asset('images/placeholder.png'),
                        'image3' => $artist->image3 ? asset('storage/'.$artist->image3) : asset('images/placeholder.png'),
                        'priority' => $artist->priority,
                        'featured' => $artist->featured,
                        'coverImage' => $artist->coverSrc,
                        'description' => $artist->description,
                    ];
                    $array_a[] = $obj_child;
                }
                $items['artists'] = $array_a;
            }


                $obj_videos = [
                    "id" => $video->id,
                    "title" => $video->title,
                    "thumbnail" => $video->imageSrc,
                    'coverImage' => $video->coverSrc,
                    "priority" => $video->priority,
                    "hash" => $video->hash(),
                    "description" => $video->description,
                    "src" => $video->src,
                    "videoURL" => $video->videoURL,
                    "platform" => $video->platform ? $video->platform : 'vimeo',
                    "items" => $items,
                    "type" => "video"
                ];

                $json[] = $obj_videos;

        }
        else {
            $json['status'] = false;
        }
        return response($json);
    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)->first();
        if ($page) {
            $obj_page = [
                'id' => $page->id,
                'slug' => $page->slug,
                'title' => $page->title,
                'content' => $page->content,
                "image" => asset('storage/'.$page->image),
            ];
            $json[] = $obj_page;
        }
        else {
            $json['status'] = false;
        }
        return response($json);
    }

    public function artist($artist_id)
    {
        $json= [];
        $artist = Artist::where('id',  $artist_id)->first();
        if ($artist) {
            $json['artist'] = [
                'artistName' => $artist->artistName,
                'logo' => $artist->logo ? asset('storage/'.$artist->logo) : asset('images/placeholder.png'),
                'image1' => $artist->image1 ? asset('storage/'.$artist->image1) : asset('images/placeholder.png'),
                'image2' => $artist->image2 ? asset('storage/'.$artist->image2) : asset('images/placeholder.png'),
                'image3' => $artist->image3 ? asset('storage/'.$artist->image3) : asset('images/placeholder.png'),
                'priority' => $artist->priority,
                'featured' => $artist->featured,
                'coverImage' => $artist->coverSrc,
                'description' => $artist->description,

            ];
                $videos = Video::whereHas('categories', function ($query) use($artist_id) {
                    $query->where('category_id',  $artist_id);
                })->orderByDesc('priority')->get();
                if ($videos) {
                    foreach ( $videos as $video ) {
                        $obj_videos = [
                            "id" => $video->id,
                            "title" => $video->title,
                            "hash" => $video->hash(),
                            "videoURL" => $video->videoURL,
                            "priority" => $video->priority,
                            "thumbnail" => $video->imageSrc,
                            'coverImage' => $video->coverSrc,
                            "type" => "video"
                        ];
                        $obj[] = $obj_videos;
                    }
                }

                // if ($children) {
                //     foreach ( $children as $child ) {
                //         $obj_child = [
                //             "id" => $child->id,
                //             "title" => $child->title,
                //             "priority" => $child->priority,
                //             "thumbnail" => $child->imageSrc,
                //             'coverImage' => $child->coverSrc,
                //             'description' => $child->description,
                //             "type" => "category"
                //         ];
                //         $obj[] = $obj_child;
                //     }
                // }

                if (isset($obj)){
                    $obj = array_values(Arr::sort($obj, function ($value) {
                        return $value['priority'];
                    }));
                    $json['תכנים'] = $obj;
                    $obj = [];
                }

        }
        else {
            $json['status'] = false;
        }
        return $json;
    }
}
