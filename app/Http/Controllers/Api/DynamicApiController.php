<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Category;
use App\Page;
use App\Video;
use App\Artist;
use App\Log;
use App\Ad;
class DynamicApiController extends Controller
{
    public function dynamic($type,$id = null, Request $request)
    {
        $platform = (!empty($request->platform) ) ? $request->platform : "";

        $webHeaderType = "cover";


        if ($type == "artist"){

            $artist = Artist::where('id',  $id)->first();
            $status = true;
            $error = "";

            if (!$artist) {
                $json = (object) [
                    "status" => false,
                    "error" => "Artist not found"
                ];
                return response()->json($json);
            }

            Log::add($artist);

            $coverImage = $artist->coverSrc;
            $title = $artist->artistname;
            $description = $artist->description;
            $videoUrl = "";
            $videoSrc = "";
            $video_id = ""; // for streaming
            $prerollVideoUrl = "";
            $prerollVideoSrc = "";
            $prerollVideoAllowSkip = false;
            $sections = [];


            // GET VIDEOS OF THIS SECTION
            $section_items = [];
            $videos = $artist->videos()->orderByDesc('priority')->get();

            $sectionTitle = "תכנים";
            // if no video, add theme song
            if (!sizeof($videos)){
                $video = Video::where('id',53)->first();
                if ($video){
                    $sectionTitle =  "בקרוב יעלו תכנים";
                    $item = $video->apiDataArr();
                    $section_items[] = $item;
                }
            }
            
            if ($videos)
            foreach($videos as $gc){
                    $section_items[] = $gc->apiDataArr();

            }

            $sections[] = (object) [
                "title" => $sectionTitle,
                "itemType" => "box",
                "sectionType"=> "posts",
                "itemSize" => 50,
                "items" => $section_items
                    
            ]; 

       
            $categories = $artist->categories()->orderByDesc('priority')->get();
         
            if ($categories) 
                foreach ( $categories as $child ) {

                    $section_items = [];
                    $grand_children = $child->children();
                    if ($grand_children)
                        foreach($grand_children as $gc){
                            $item = [
                                "id" => $gc->id,
                                "title" => $gc->title,
                                "thumbnail" => $gc->imageSrc,
                                "type" => "category"
                            ];
                            $section_items[] = $item;

                    }
                    
                    $videos = $child->videos()->orderByDesc('priority')->get();
                    if ($videos)
                        foreach($videos as $gc){
                            // $item = [
                            //     "id" => $gc->id,
                            //     "title" => $gc->title,
                            //     "thumbnail" => $gc->imageSrc,
                            //     "type" => "video"
                            // ];
                            $section_items[] = $gc->apiDataArr();

                    }


                    $sections[] = (object) [
                        "title" => $child->title,
                        "itemType" => "box",
                        "itemSize" => 50,
                        "items" => $section_items
                            
                    ];    

            }


                

            $json = (object) [
                "status" => true,
                "coverImage" => $coverImage,
                "title" => $title,
                "description" => $description,
                "videoUrl" => $videoUrl,
                "videoSrc" => $videoSrc,
                "prerollVideoUrl" => $prerollVideoUrl,
                "prerollVideoSrc" => $prerollVideoSrc,
                "prerollVideoAllowSkip" => $prerollVideoAllowSkip,
                "sections" => $sections
            ];

            return response()->json($json);

            
        }


         
        if ($type == "home"){
            $status = true;
            $json['home-video'] = ['title' => setting('site.homeTitle'), 'img'=> asset('storage/'.setting('site.homeImage')), 'description' => setting('site.homeDescription'),  'url'=>setting('site.homeVideoUrl')] ;


            $coverImage = asset('storage/'.setting('site.homeImage'));
            $title = setting('site.homeTitle');
            $description = setting('site.homeDescription');
            $videoUrl = setting('site.homeVideoUrl');
            $videoSrc = setting('site.homeVideoSrc');
            $video_id = setting('site.homeVideoId'); // for streaming
            $prerollVideoUrl = "";
            $prerollVideoSrc = "";
            $prerollVideoAllowSkip = false;


            // BUILD ARTIST ITEMS
            $featured_artists_arr = [];
            $artists = Artist::whereNotNull('featured')->orderByDesc('featured','priority')->get();
            if ($artists)
            foreach ( $artists as $artist ) {
                // $item = [
                //     "thumbnail" => $artist->logoSrc,
                //     "title" => $artist->artistname, 
                //     "id" => $artist->id,
                //     "type" => "artist",

                // ];
                $featured_artists_arr[] =  $artist->apiDataArr();
            }
            
             // BUILD ARTIST ITEMS
             $artists_arr = [];
             $artists = Artist::orderByDesc('priority')->get();
             if ($artists)
             foreach ( $artists as $artist ) {
                //  $item = [
                //      "thumbnail" => $artist->logoSrc,
                //      "title" => $artist->artistname, 
                //      "id" => $artist->id,
                //      "type" => "artist",
 
                //  ];
                 $artists_arr[] =  $artist->apiDataArr();
             }
 



            $sections = [];

            $banners_arr = [];

            foreach(Ad::get('topbanner1',1) as $ad){
                $banners_arr[] = $ad->apiDataArr();
            }

            foreach(Ad::get('topbanner2',1) as $ad){
                $banners_arr[] = $ad->apiDataArr();

            }

            $sections[] = (object) [
                "title" => "", // artists
                "sectionType" => "banners",
                "itemType" => "banner",
                "itemSize" => null,
                "items" => $banners_arr
                    
            ];


            $sections[] = (object) [
                "title" => "הכוכבים", // artists
                "itemType" => "circle",
                "sectionType" => "float",
                "webType" => "circles",
                "itemSize" => 70,
                "items" => $artists_arr
                    
            ];


            $categories = Category::whereNull('parent_id')->orderByDesc('priority')->get();

            if ($categories) 
                foreach ( $categories as $child ) {

                    $section_items = [];
                    $grand_children = $child->children();
                    if ($grand_children)
                        foreach($grand_children as $gc){
                            $item = [
                                "id" => $gc->id,
                                "title" => $gc->title,
                                "thumbnail" => $gc->imageSrc,
                                "type" => "category"
                            ];
                            $section_items[] = $gc->apiDataArr();

                            //$section_items[] = $item;

                    }
                    
                    $videos = $child->videos()->orderByDesc('priority')->get();
                    if ($videos)
                        foreach($videos as $gc){
                            $section_items[] = $gc->apiDataArr();
                    }


                    $sections[] = (object) [
                        "title" => $child->title,
                        "itemType" => "box",
                        "sectionType"=> "posts",
                        "itemSize" => 50,
                        "items" => $section_items
                            
                    ];    

            }



            $banners_arr = [];

            foreach(Ad::get('banner1',1) as $ad){
                $banners_arr[] = $ad->apiDataArr();
            }

            foreach(Ad::get('banner2',1) as $ad){
                $banners_arr[] = $ad->apiDataArr();

            }
            foreach(Ad::get('banner3',1) as $ad){
                $banners_arr[] = $ad->apiDataArr();
            }

            $sections[] = (object) [
                "title" => "", // artists
                "sectionType" => "banners",
                "itemType" => "banner",
                "itemSize" => null,
                "items" => $banners_arr
                    
            ];

                        
            $json = (object) [
                "status" => true,
                "coverImage" => $coverImage,
                "webHeaderType" => $webHeaderType,
                "title" => $title,
                "description" => $description,
                "videoUrl" => $videoUrl,
                "videoSrc" => $videoSrc,
                "video_id" => $video_id,
                "prerollVideoUrl" => $prerollVideoUrl,
                "prerollVideoSrc" => $prerollVideoSrc,
                "prerollVideoAllowSkip" => $prerollVideoAllowSkip,
                "sections" => $sections
            ];

            return response()->json($json);

            
        }




        if ($type == "category"){

            $category = Category::where('id',  $id)->with(['artists', 'videos'])->first();
            $status = true;
            $error = "";

            if (!$category) {
                $json = (object) [
                    "status" => false,
                    "error" => "Category not found"
                ];
                return response()->json($json);
            }

            Log::add($category);

            $coverImage = $category->coverSrc;
            $title = $category->title;
            $description = $category->description;
            $videoUrl = "";
            $videoSrc = "";
            $video_id = ""; // for streaming
            $prerollVideoUrl = "";
            $prerollVideoSrc = "";
            $prerollVideoAllowSkip = false;
            $sections = [];

            // GET VIDEOS OF THIS SECTION
            $section_items = [];
            $videos = $category->videos()->orderByDesc('priority')->get();
            if ($videos)
                foreach($videos as $gc){
                    $section_items[] = $gc->apiDataArr();


            }

            $sections[] = (object) [
                "title" => "תכנים",
                "itemType" => "box",
                "sectionType"=> "posts",
                "itemSize" => 50,
                "items" => $section_items
                    
            ]; 

            // get all sub categories as sections

       
            $children = $category->children();

            if ($children) 
                foreach ( $children as $child ) {

                    $section_items = [];
                    $grand_children = $child->children();
                    if ($grand_children)
                        foreach($grand_children as $gc){
                            $item = [
                                "id" => $gc->id,
                                "title" => $gc->title,
                                "thumbnail" => $gc->imageSrc,
                                "type" => "category"
                            ];
                            $section_items[] = $item;

                    }
                    
                    $videos = $child->videos()->orderByDesc('priority')->get();
                    if ($videos)
                        foreach($videos as $gc){
                            $section_items[] = $gc->apiDataArr();
                    }


                    $sections[] = (object) [
                        "title" => $child->title,
                        "itemType" => "box",
                        "sectionType"=> "posts",
                        "itemSize" => 50,
                        "items" => $section_items
                            
                    ];    

            }
                

            $json = (object) [
                "status" => true,
                "coverImage" => $coverImage,
                "title" => $title,
                "description" => $description,
                "videoUrl" => $videoUrl,
                "videoSrc" => $videoSrc,
                "prerollVideoUrl" => $prerollVideoUrl,
                "prerollVideoSrc" => $prerollVideoSrc,
                "prerollVideoAllowSkip" => $prerollVideoAllowSkip,
                "sections" => $sections
            ];

            return response()->json($json);

            
        }

        
        if ($type == "video"){
            $video = Video::where('id',  $id)->with(['artists', 'categories'])->first();
            $status = true;
            $error = "";

            if (!$video) {
                $json = (object) [
                    "status" => false,
                    "error" => "Video not found"
                ];
                return response()->json($json);
            }

            Log::add($video);
            $coverImage = $video->coverSrc;
            $title = $video->title;
            $description = $video->description;
            $videoUrl = $video->videoURL;
            $videoSrc = $video->src;
            $video_id = ""; // for streaming
            $prerollVideoUrl = "";
            $prerollVideoSrc = "";
            $youtube_video_id = "";
            // we use youtube if we dont have videoSrc 
            if ($videoSrc == "" && $video->youtube_video_id){
                $youtube_video_id = $video->youtube_video_id;
                $videoSrc = "https://www.youtube.com/embed/{$video->youtube_video_id}?autoplay=1";
            }

            if ($platform == "android"){
                $youtube_video_id = trim($video->youtube_video_id);
            }
            
            $prerollVideoAllowSkip = false;
            $autoplay = true;
            // BUILD ARTIST ITEMS
            $artists_arr = [];
            $artists = $video->artists()->orderByDesc('featured','priority')->get();
            if ($artists)
            foreach ( $artists as $artist ) {
                $item = [
                    "thumbnail" => $artist->logoSrc,
                    "title" => $artist->artistname, 
                    "id" => $artist->id,
                    "type" => "artist",

                ];
                $artists_arr[] = $item;
            }


            // BUILD RELATED VIDEOS ITEMS
            $related_videos_arr = [];
            $related_videos = Video::take(100)->get()->shuffle()->take(10);

            if ($related_videos)
            foreach ( $related_videos as $video ) {

                $related_videos_arr[] = $video->apiDataArr();

            }


            $sections = [];

            if ( sizeof($artists_arr))
                $sections[] = (object) [
                    "title" =>  sizeof($artists_arr) > 1 ? "הכוכבים" : "", // artists
                    "itemType" => "circle",
                    "itemSize" => 70,
                    "items" => $artists_arr
                        
                ];

 

            $sections[] = (object) [
                "title" => "תכנים נוספים בשבילך", // more videos for you
                "itemType" => "box",
                "sectionType"=> "posts",
                "itemSize" => null,
                "items" => $related_videos_arr
                    
            ];
                        

            $json = (object) [
                "status" => true,
                "coverImage" => $coverImage,
                "title" => $title,
                "description" => $description,
                "videoUrl" => $videoUrl,
                "videoSrc" => $videoSrc,
                "youtubeId" => $youtube_video_id,
                "videoAutoPlay" => true,
                "prerollVideoUrl" => $prerollVideoUrl,
                "prerollVideoSrc" => $prerollVideoSrc,
                "prerollVideoAllowSkip" => $prerollVideoAllowSkip,
                "sections" => $sections
            ];

            return response()->json($json);

            
        }



    }
}
