<?php

namespace App\Http\Controllers\Api;

use App\Device;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\DeviceFormRequest;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function device(DeviceFormRequest $request){
        $device = Device::updateOrCreate([
            'token' => $request->token,
            'os' => $request->os,
        ],[
            'token' => $request->token,
            'os' => $request->os,
            'version' => $request->version,
            'app_version' => $request->app_version,

        ]);
        return response($device);
    }
}
