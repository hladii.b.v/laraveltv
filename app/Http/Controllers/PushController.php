<?php

namespace App\Http\Controllers;

use App\Device;
use App\Jobs\SendPush;
use App\Jobs\SendPushWithItem;
use App\Jobs\SendTestPush;
use App\Jobs\SendTestPushWithItem;
use Edujugon\PushNotification\Facades\PushNotification;
use Illuminate\Http\Request;

class PushController extends Controller
{
    public function send()
    {
        return view('vendor.voyager.push.send');
    }

    public function store(Request $request)
    {
        $this->authorize('browse_admin');

        if ($request->has('test')){
            $devices = Device::whereHas('users', function($q){
                    $q->where('role_id', 1);
                })
                ->get()
                ->unique('token')
                ->pluck('token')
                ->toArray();
        }

        if ($request->has(['type','item_id'])){
            if(($request->type != "") && ($request->item_id != "")){
                if ($request->has('test')){
                    $push = PushNotification::setService('fcm') // change as per your requirement FCM / GCM / APN
                        ->setMessage([
                            'notification' => [
                                'title'=> $request->title ?? '',
                                'body'=> $request->text ?? '',
                                'sound' => 'default'
                            ],
                            'data' => [
                                'type' => $request->type ?? '',
                                'item_id' => $request->item_id ?? '',
                            ]
                        ])
                        ->setDevicesToken($devices)
                        ->send()
                        ->getFeedback();
//                    dd($push);
                } else {
                        $push = PushNotification::setService('fcm') // change as per your requirement FCM / GCM / APN
                        ->setMessage([
                            'notification' => [
                                'title'=> $request->title ?? '',
                                'body'=> $request->text ?? '',
                                'sound' => 'default'
                            ],
                            'data' => [
                                'type' => $request->item_id ?? '',
                                'item_id' => $request->type ?? '',
                            ]
                        ])
                        ->sendByTopic('all_users')
                        ->getFeedback();
//                    dd($push);
                }

            } else {
                if ($request->has('test')){
                    $push = PushNotification::setService('fcm') // change as per your requirement FCM / GCM / APN
                    ->setMessage([
                        'notification' => [
                            'title'=> $request->title ?? '',
                            'body'=> $request->text ?? '',
                            'sound' => 'default'
                        ]
                    ])
                        ->setDevicesToken($devices)
                        ->send()
                        ->getFeedback();
//                    dd($push);
                } else {
                    $push = PushNotification::setService('fcm') // change as per your requirement FCM / GCM / APN
                    ->setMessage([
                        'notification' => [
                            'title'=> $request->title ?? '',
                            'body'=> $request->text ?? '',
                            'sound' => 'default'
                        ]
                    ])
                        ->sendByTopic('all_users')
                        ->getFeedback();
//                    dd($push);
                }
            }
        }

        return redirect()->route('voyager.dashboard');

//        $devices = Device::get()
//            ->unique('token')
//            ->pluck('token')
//            ->chunk(500)
//            ->toArray();
//
//        $iteration = 0;
//        foreach ($devices as $tokens){
//            $iteration += 1;
//            SendPushWithItem::dispatch($request->all(), array_values($tokens), $item)->delay(now()->addMinutes($iteration));
//            MyLog::create([
//                'user_id' => null,
//                'title' => "push - $iteration",
//                'data' => json_encode(['requests' => $request->all(), 'Iteration' => $iteration, 'tokens' => $tokens]),
//            ]);
//        }


//        $fields = array();
//
//        $target = 'all_users';
//
//        $url = 'https://fcm.googleapis.com/fcm/send';
//        //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
//        $server_key = 'AAAAIKLIisI:APA91bEjlLgOqPvR1oc4fAoR_Xq8JDD_aRrzWn6P5Q2ZUKBXZ92Qo0itxmn1LFW_OzpPhu0Op0UnFEebkcNQz1SKS70j4j7heeuK5sgH8biH-kumDSmLRPNqBgqrAjn3N-RGlEk9lOv2';
//
//        if ($request->has(['type','item_id'])){
//            if($request->type  != null && $request->item_id != null){
//                $fields['data'] = [
//                    'type' => $request->type,
//                    'item_id' => $request->item_id
//                ];
//            }
//        }
//
//        $fields['notification'] = [
//            "body" => $request->text,
//            "title" => $request->title,
//            'vibrate' => true,
//            'sound' => true,
//        ];
//
//        if(is_array($target)){
//            $fields['registration_ids'] = $target;
//        }else{
//            $fields['topic'] = $target;
//            $fields['to'] = '/topics/'.$target;
//        }
//
//        //header with content_type api key
//        $headers = array(
//            'Content-Type:application/json',
//            'Authorization:key='.$server_key
//        );
//
//        $ch = curl_init();
//        curl_setopt($ch, CURLOPT_URL, $url);
//        curl_setopt($ch, CURLOPT_POST, true);
//        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
//        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
//        $result = curl_exec($ch);
//        if ($result === FALSE) {
//            die('FCM Send Error: ' . curl_error($ch));
//        }
//        curl_close($ch);
//        return $result;
////        return view('vendor.voyager.push.send');

    }
}
