<?php

namespace App\Http\Controllers;

use App\Category;
use App\Video;
use App\Artist;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Api\DynamicApiController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function indexOld()
    {

        $apiController = new ApiController;

        $homeContent = $apiController->home();
        $data = json_decode($homeContent->getContent());


        $meta = [
            "pageTitle" => "יחד TV - האתר הרשמי" ,
            "pageKeywords" =>  "ברוכים הבאים לאתר הרשמי של ערוץ יחד TV ",
            "pageDescription" => "ברוכים הבאים לאתר הרשמי של ערוץ יחד TV "
        ];


        return view('home',compact('data','meta'));

    }


    public function index()
    {

        $apiController = new ApiController;

        $homeContent = $apiController->home();
        $data = json_decode($homeContent->getContent());


        $meta = [
            "pageTitle" => "יחד TV - האתר הרשמי" ,
            "pageKeywords" =>  "ברוכים הבאים לאתר הרשמי של ערוץ יחד TV ",
            "pageDescription" => "ברוכים הבאים לאתר הרשמי של ערוץ יחד TV "
        ];


        return view('pages.home',compact('data','meta'));

    }

    public function isMobile(){
            return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
    }

    public function dynamicHome()
    {

        $apiController = new DynamicApiController;
        $request = new \Illuminate\Http\Request();

        $result = $apiController->dynamic("home",1, $request );
        $data = json_decode($result->getContent());

        $meta = [
            "pageTitle" => "יחד TV - האתר הרשמי" ,
            "pageKeywords" =>  "יחד tv,ערוץ יחד, יחד,yachad,yahad,yahadtv",
            "pageDescription" => "ברוכים הבאים לאתר הרשמי של ערוץ יחד TV "
        ];
        $type = "home";
        $id = 1;
        $isMobile = $this->isMobile();
        return view('pages.dynamic',compact('data','meta','type','id','isMobile'));

    }

    public function dynamic($type,$id)
    {

        $apiController = new DynamicApiController;
        $request = new \Illuminate\Http\Request();

        $result = $apiController->dynamic($type,$id , $request  );
        $data = json_decode($result->getContent());

        $pageTitle = "יחד TV - האתר הרשמי" ;
        $pageKeywords = "יחד tv,ערוץ יחד, יחד,yachad,yahad,yahadtv";
        $pageDescription = "ברוכים הבאים לאתר הרשמי של ערוץ יחד TV ";

        $pageTitle = $data->title." - יחד TV";
        $pageDescription = $data->title." - ".$data->description." - יחד TV";
        $pageKeywords = $data->title;

        $meta = [
            "pageTitle" => $pageTitle ,
            "pageKeywords" =>  $pageKeywords,
            "pageDescription" => $pageDescription
        ];
        $isMobile = $this->isMobile();

        return view('pages.dynamic',compact('data','meta','type','id','isMobile'));

    }

}
