<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;
use App\Artist;
use App\Category;

class VideoController extends Controller
{
     public function all_videos()
     {
         $videos = Video::orderByDesc('priority')->get();
         return view('videos')->withVideos($videos);
     }
    public function index(Request $request, Video $video)
    {
        $categories = Category::whereHas('videos', function ($query) use($video) {
            $query->where('video_id',  $video->id);
        })->orderByDesc('priority')->get();
        $artists = Artist::whereHas('videos', function ($query) use($video) {
            $query->where('artist_id',  $video->id);
        })->orderByDesc('priority')->get();
        $videos = Video::where('id',  $video->id)->first();
        return view('single-video')->withVideos($videos)->withCategories($categories)->withArtists($artists);
    }
}
