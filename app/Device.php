<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = [
        'user_id', 'os', 'token', 'version', 'app_version',
    ];

//    public function scopeActive($query, $option)
//    {
//        return $query->where('status', $option);
//    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
