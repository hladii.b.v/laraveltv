<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = ['user_id', 'loggable_type', 'loggable_id','data'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function loggable()
    {
        return $this->morphTo();
    }

    public static function add(Object $object,$data="",$user_id=null){
        // if (empty($user_id)) $user_id = Auth::id();
        
        // if (!$user_id) return false;

        $loggable_type = get_class($object);
        $loggable_id = $object->id;

        $log = new Log();

//         $log = Log::firstOrNew([
//             'user_id' => $user_id , 
//             'loggable_type'=> $loggable_type,
//             'loggable_id' => $loggable_id,
// //            'log' => $log_kind
//         ]);

        $log->user_id = $user_id;
        $log->loggable_type = $loggable_type;
        $log->loggable_id = $loggable_id;
        $log->data = $data;
        $log->save();
        return true;
        
    }
}
