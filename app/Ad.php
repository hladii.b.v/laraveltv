<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Request;
use Carbon\Carbon;
use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;
use DB;
class Ad extends Model
{
    use SoftDeletes;        
    use Resizable;
    
    public function scopeActive($query){
            
            $query = $query->where('date_from' ,'<=' , Carbon::now() )
                    ->where('date_to' ,'>=' , Carbon::now() );
            return $query;
    }

    public function getImageSrcAttribute(){
        return asset("storage/".$this->image);
    }
    public function getTrackedLinkAttribute(){
        return $this->link ? $this->link : "#";
    }

    public function apiDataArr(){
        $item = [
            "id" => $this->id,
            "title" => $this->title,
            "subtitle" => "",
            "thumbnail" => $this->imageSrc,
            "cover" => $this->imageSrc,
            "url" => $this->getTrackedLinkAttribute(),
            "type" => "banner"
        ];
        return $item;
    }

    public static function get($position,$num=null,$shuffle=true){

        $page = request()->has('type') ? request()->get('type') : 1;        
        $tag = "ads {$page} {$position} - {$num} - {$shuffle} ";
        return Cache::remember($tag, 1 , function () use ($position,$num,$shuffle)
        { 

            $segments = Request::segments();
            if (!$segments) $segments[]='home';
            $positions = explode(",",$position);
            $ads = Ad::active()
            ->where(function($query) use ($positions){
                $query->whereNull('position');            
                foreach($positions as $position)
                $query->orWhere('position','like', '%' . $position . '%');
                }) 
            ->where(function($query) use ($segments){
            $query->whereNull('pages');
                $query->orWhere('pages','');
                foreach($segments as $segment)
                    if (!is_numeric($segment))
                        $query->orWhere('pages','like', '%' . $segment . '%');
                })->get();
        
            $ads = $ads->shuffle();
            //if ($num == 1) return $ads->first();
            if ($num > 0) $ads = $ads->take($num);

            return $ads;
        });         
    }
}
