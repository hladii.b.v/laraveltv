<?php

namespace App\Jobs;

use Edujugon\PushNotification\Facades\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendTestPushWithItem implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $item_id;
    protected $type;
    protected $text;
    protected $tokens;
    public $tries = 3;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $text, $item_id, $type, $tokens)
    {
        $this->title = $title;
        $this->text = $text;
        $this->item_id = $item_id;
        $this->type = $type;
        $this->tokens = $tokens;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PushNotification::setService('fcm') // change as per your requirement FCM / GCM / APN
            ->setMessage([
                'notification' => [
                    'title'=> $this->title ?? '',
                    'body'=> $this->text ?? '',
                    'sound' => 'default'
                ],
                'data' => [
                    'type' => $this->type ?? '',
                    'item_id' => $this->item_id ?? '',
                ]
            ])
            ->setDevicesToken($this->tokens)
            ->send()
            ->getFeedback();
    }
}
