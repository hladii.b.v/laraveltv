<?php

namespace App\Jobs;

use Edujugon\PushNotification\Facades\PushNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendPush implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    protected $title;
    protected $text;
    public $tries = 3;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($title, $text)
    {
        $this->title = $title;
        $this->text = $text;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PushNotification::setService('fcm') // change as per your requirement FCM / GCM / APN
            ->setMessage([
                'notification' => [
                    'title'=> $this->title ?? '',
                    'body'=> $this->text ?? '',
                    'sound' => 'default'
                ]
            ])
            ->sendByTopic('all_users')
            ->getFeedback();
    }
}
