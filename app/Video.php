<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'thumbnail', 'start_at', 'end_at', 'priority', 'hash', 'description', 'src', 'videoURL', 'platform','youtube_video_id'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'video_category');
    }


    public function thumbSrc(){
        $imgid = $this->id;

        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$imgid.php"));
        
        $src=  $hash[0]['thumbnail_medium'];  
        return $src ? $src : asset('images/placeholder.png');

    }

    public function artists()
    {
        return $this->belongsToMany(Artist::class, 'videos_artist');
    }

    public function artistName(){
        $artist = $this->artists()->first();
        if ($artist) return $artist->artistname;
        return "";
    }

    public function apiDataArr(){
        $item = [
            "id" => $this->id,
            "title" => $this->title,
            "subtitle" => $this->artistName(),
            "thumbnail" => $this->imageSrc,
            "cover" => $this->coverSrc,
            "type" => "video"
        ];
        return $item;
    }

    public function hash(){
        return sha1($this->id . "YAHADTV");
    }

    public function getImageSrcAttribute(){

        if ($this->youtube_video_id>"")
           return "https://img.youtube.com/vi/{$this->youtube_video_id}/0.jpg";    
 
        $src = $this->thumbnail;

        if (!$src){

            $category = $this->categories()->first();
            if ($category){
                if ($category->thumbnail) $src = $category->imageSrc;
            }

            if (!$src){   
                $artist = $this->artists()->first();
                if ($artist){
                  if ($artist->logo) $src = $artist->logoSrc;
                }
    
            } 

            
        } else{
            $src = asset('storage/'.$this->thumbnail); 
        }
        
    
   
        return $src ? $src : asset('images/placeholder-dark.jpg');
    }

    public function getCoverSrcAttribute(){
        if ($this->youtube_video_id>"")
          return "https://img.youtube.com/vi/{$this->youtube_video_id}/0.jpg";
        return $this->coverImage ? asset('storage/'.$this->coverImage) : asset('images/placeholder-dark.jpg');
    }

}
