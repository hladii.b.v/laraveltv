<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories', function (Blueprint $table) {
            $table->string('thumbnail')->nullable();
            $table->string('coverImage')->nullable();
        });

        Schema::create('artists', function (Blueprint $table) {
            $table->id();
            $table->string('artistName')->nullable();
            $table->string('logo')->nullable();
            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();
            $table->integer('priority')->nullable();
            $table->integer('featured')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('id')->references('id')->on('videos')->onDelete('cascade');
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->string('description')->nullable();
            $table->string('src')->nullable();
            $table->string('videoURL')->nullable();
            $table->string('platform')->nullable();
            $table->bigInteger('artist_id')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artists');

    }
}
