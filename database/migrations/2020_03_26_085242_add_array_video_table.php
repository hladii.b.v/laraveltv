<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddArrayVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('videos')->insert(
            array(
                'title' => 'מיכל הקטנה',
                'thumbnail' => 'images/מיכל הקטנה.jpg',
                'priority' => 3
            )
        );
        DB::table('videos')->insert(
            array(
                'title' => 'מיקי',
                'thumbnail' => 'images/מיקי.png',
                'priority' => 4
            )
        );
        DB::table('videos')->insert(
            array(
                'title' => 'מר עגבניה',
                'thumbnail' => 'images/מר עגבניה.jpg',
                'priority' => 5
            )
        );
        DB::table('videos')->insert(
            array(
                'title' => 'קופיקו חוקר הטבע',
                'thumbnail' => 'images/קופיקו חוקר הטבע.jpg',
                'priority' => 3
            )
        );
        DB::table('videos')->insert(
            array(
                'title' => 'רוי בוי ביער הסודי',
                'thumbnail' => 'images/רוי בוי ביער הסודי.jpg',
                'priority' => 2
            )
        );
        DB::table('videos')->insert(
            array(
                'title' => 'שי ורועי השטותריקים',
                'thumbnail' => 'images/שי ורועי השטותריקים.jpg',
                'priority' => 1
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
