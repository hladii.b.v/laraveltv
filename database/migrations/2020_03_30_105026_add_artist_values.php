<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddArtistValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('artists')->insert(
            array(
                'artistName' => 'Artsst1',
                'priority' => 3
            )
        );
        DB::table('artists')->insert(
            array(
                'artistName' => 'Artsst2',
                'priority' => 1
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 1,
                'artist_id' => 1
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 2,
                'artist_id' => 1
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 3,
                'artist_id' => 1
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 4,
                'artist_id' => 1
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 5,
                'artist_id' =>2
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 6,
                'artist_id' => 2
            )
        );
        DB::table('videos_artist')->insert(
            array(
                'video_id' => 7,
                'artist_id' => 2
            )
        );
        DB::table('category_artist')->insert(
            array(
                'category_id' => 1,
                'artist_id' => 1
            )
        );
        DB::table('category_artist')->insert(
            array(
                'category_id' => 2,
                'artist_id' => 1
            )
        );
        DB::table('category_artist')->insert(
            array(
                'category_id' => 2,
                'artist_id' => 2
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
