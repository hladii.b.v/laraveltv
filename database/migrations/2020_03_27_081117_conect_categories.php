<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConectCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('videos')->insert(
            array(
                'title' => 'כל הכוכבים',
                'thumbnail' => 'images/כל הכוכבים.jpg',
                'priority' => 1
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 1,
                'category_id' => 1
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 2,
                'category_id' => 2
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 3,
                'category_id' => 1
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 4,
                'category_id' => 2
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 5,
                'category_id' => 1
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 6,
                'category_id' => 2
            )
        );
        DB::table('video_category')->insert(
            array(
                'video_id' => 7,
                'category_id' => 1
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
