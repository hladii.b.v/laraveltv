<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/landing',  'IndexController@index')->name('landing');
Route::get('/get',  'IndexController@getApp')->name('getApp');
Route::get('/contact',  'IndexController@contact')->name('contact');
Route::post('/contact',  'IndexController@ContactStore')->name('contact.store');

Route::get('/',  'HomeController@dynamicHome')->name('home_new');

Route::get('/page/{page}', 'PageController')->name('page.show');


Route::get('/content/{type}/{id}',  'HomeController@dynamic')->name('dynamic');


Route::get('/artists',  'ArtistController@all_artists')->name('all-artists');

// Route::get('/artist/{artist?}', function ( $artist= 1) {
//     return $artist;
// })->uses  ('ArtistController@index')->name('artist');

Route::get('/videos',  'VideoController@all_videos')->name('all-videos');

// Route::get('/video/{video?}', function ($video = 1) {
//     return $video;
// })->uses  ('VideoController@index')->name('single-video');

Route::get('/categories/{category?}', function ($category = 1) {
    return $category;
})->uses('CategoriesController@index')->name('categories');

Route::resource('subscription','SubscriptionController');


Auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('push','PushController@send')->middleware(['web','admin.user'])->name('voyager.push.send');
    Route::post('push','PushController@store')->middleware(['web','admin.user'])->name('voyager.push.store');
});
