<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'device'], function() {
    Route::post('create','ProfileController@device');
});

Route::group(['namespace' => 'Api'], function() {
    Route::group(['prefix' => 'home'], function() {
        Route::post('/','ApiController@home');
    });
    Route::post('/category/{category}',  'ApiController@category');

    Route::post('/artist/{artist}',  'ApiController@artist');

    Route::post('/video/{video}/{hash}',  'ApiController@video');
    Route::post('/page/{slug}',  'ApiController@page');

    Route::post('/dynamic/{type}/{id}',  'DynamicApiController@dynamic');


    Route::group(['prefix' => 'device'], function() {
        Route::post('create','DeviceController@device');
    });

    Route::post('/app/version')->uses('ApplicationController@version');
});

