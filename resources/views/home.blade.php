@extends('layouts.app')
@section('pageTitle', $meta['pageTitle'])
@section('pageDescription', $meta['pageDescription'])
@section('pageKeywords', $meta['pageKeywords'] )
@section('content')




    <main id="col-main">

        <div class="flexslider progression-studios-dashboard-slider">

            <ul class="slides">
                <li class="progression_studios_animate_left">
                    <div class="progression-studios-slider-dashboard-image-background" id="homebanner"  style="background-image:url({{ asset('storage/'.setting('site.homeImage'))}});">
                        <div class="progression-studios-slider-display-table">
                            <div class="progression-studios-slider-vertical-align">
                                <div  >

                                </div>

                                <a id="myBtn" class="progression-studios-slider-play-btn afterglow" ><i class="fas fa-play  font-icon"></i></a>
                            {{--<button id="myBtn">Open Modal</button>--}}

                            <!-- The Modal -->
                                <div id="myModal" class="modal">

                                    <!-- Modal content -->
                                    <div class="modal-content">
                                        <span class="close">&times;</span>
                                        <iframe  id="VideoLightbox-1" src="{{ setting('site.homeVideoSrc') }}" width="800" height="400" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

                                    </div>

                                </div>

                                <div class="container">


                                    <div
                                        class="circle-rating-pro"
                                        data-value="0.86"
                                        data-animation-start-value="0.86"
                                        data-size="70"
                                        data-thickness="6"
                                        data-fill="{
								          &quot;color&quot;: &quot;#42b740&quot;
								        }"
                                        data-empty-fill="#def6de"
                                        data-reverse="true"
                                    ><span style="color:#42b740;">8.6</span></div>

                                    <div class="progression-studios-slider-dashboard-caption-width">
                                        <div class="progression-studios-slider-caption-align">


                                            <h2><a href="#">ברוכים הבאים ליחד TV!</a></h2>
                                            <ul class="progression-studios-slider-meta">

                                            </ul>


                                            <a class="btn btn-green-pro btn-slider-pro btn-shadow-pro afterglow" href="#VideoLightbox-1"></a>

                                            <div class="clearfix"></div>



                                        </div><!-- close .progression-studios-slider-caption-align -->
                                    </div><!-- close .progression-studios-slider-caption-width -->

                                </div><!-- close .container -->

                            </div><!-- close .progression-studios-slider-vertical-align -->
                        </div><!-- close .progression-studios-slider-display-table -->

                        <div class="progression-studios-slider-mobile-background-cover"></div>
                    </div><!-- close .progression-studios-slider-image-background -->
                </li>


            </ul>
        </div><!-- close .progression-studios-slider - See /js/script.js file for options -->


        <div class="clearfix"></div>

        @foreach($data as $catName=>$items)
            @if($catName !='home-video')
            <div class="dashboard-container testimonial-group">
                <h4 class="heading-extra-margin-bottom">{{ $catName }}</h4>
                <div class="row  text-center flex-nowrap">
                    @if (is_array($items) && sizeof($items))
                        @foreach($items as $item)
                            @php
                                $route = "#";
                                if ($item->type == "video" ) $route = route('single-video', $item->id);
                                if ($item->type == "category" ) $route = route('categories', $item->id);
                                if ($item->type == "artist" ) $route = route('artist', $item->id);

                            @endphp


                            <div class="col-3 col-sm-4 col-md-3 col-lg-3 col-xl-3" >
                                <div class="item-listing-container-skrn">
                                    <a href="{{ $route }}">
                                        <figure class="cover-img"><img src="{{$item->thumbnail}}" alt="{{$item->title}}"></figure>
                                    </a>
                                    <div class="item-listing-text-skrn">
                                        <div class="item-listing-text-skrn-vertical-align"><h6><a href="{{ $route }}">{{$item->title}}</a></h6>
                                            <div
                                                class="circle-rating-pro"
                                                data-value="0.86"
                                                data-animation-start-value="0.86"
                                                data-size="32"
                                                data-thickness="3"
                                                data-fill="{
                                          &quot;color&quot;: &quot;#42b740&quot;
                                        }"
                                                data-empty-fill="#def6de"
                                                data-reverse="true"
                                            ><span style="color:#42b740;">8.6</span></div>
                                        </div><!-- close .item-listing-text-skrn-vertical-align -->
                                    </div><!-- close .item-listing-text-skrn -->
                                </div><!-- close .item-listing-container-skrn -->
                            </div><!-- close .col -->

                        @endforeach
                    @endif
                </div>


            </div>
            @endif
        @endforeach


    </main>



@endsection
