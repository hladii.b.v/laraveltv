@extends('layouts.app')

@section('content')

    <main id="col-main">

        <div class="dashboard-container">

            <ul class="dashboard-sub-menu">

                @foreach($ctegories as $cat)
                    <li class="{{ ( $cat->id == $ctegory) ? 'current' : '' }} "><a href="{{route('categories', $cat->id)}}">{{ $cat->title}}</a></li>
                @endforeach
            </ul><!-- close .dashboard-sub-menu -->
            @if($videos->count() >=1 )
                <div class="movie-details-section">
                    <h2>תכנים</h2>
            <div class="row">
                @foreach($videos as $video)
                    <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                        <div class="item-listing-container-skrn">
                            <a href="{{route('single-video', $video->id)}}"><figure class="cover-img"><img src="{{ $video->imageSrc }}" alt="{{ $video->title }}"></figure></a>
                            <div class="item-listing-text-skrn">
                                <div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('single-video', $video->id)}}">{{ $video->title}}</a></h6>
                                    <div
                                        class="circle-rating-pro"
                                        data-value="0.86"
                                        data-animation-start-value="0.86"
                                        data-size="32"
                                        data-thickness="3"
                                        data-fill="{
							          &quot;color&quot;: &quot;#42b740&quot;
							        }"
                                        data-empty-fill="#def6de"
                                        data-reverse="true"
                                    ><span style="color:#42b740;">8.6</span></div>
                                </div><!-- close .item-listing-text-skrn-vertical-align -->
                            </div><!-- close .item-listing-text-skrn -->
                        </div><!-- close .item-listing-container-skrn -->
                    </div><!-- close .col -->

                @endforeach
            </div>
            </div><!-- close .row -->
            @endif
            @if($artists->count() >=1)
            <div class="movie-details-section">
                <h2>אמנים</h2>
                <div class="row">
                    @foreach($artists as $artist)
                        <div class="col-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="item-listing-container-skrn">
                                <a href="{{route('artist', $artist->id)}}"><figure class="cover-img"><img src="{{$artist->logoSrc}}" alt="{{$artist->artistName}}"></figure></a>
                                <div class="item-listing-text-skrn item-listing-movie-casting">
                                    <h6><a href="{{route('artist', $artist->id)}}">{{$artist->artistName}}</a></h6>
                                    <div class="movie-casting-sub-title">{{$artist->artistNam}}</div>
                                </div><!-- close .item-listing-text-skrn -->
                            </div><!-- close .item-listing-container-skrn -->
                        </div><!-- close .col -->
                    @endforeach
                </div>
            </div><!-- close .movie-details-section -->
            @endif

        </div><!-- close .dashboard-container -->
    </main>

@endsection
