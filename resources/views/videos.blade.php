@extends('layouts.app')

@section('content')




    <main id="col-main">


        <div class="clearfix"></div>

        @if($videos->count() >=1 )
            <div class="dashboard-container ">

                <h4 class="heading-extra-margin-bottom">תכנים</h4>
                <div class="row ">

                    @foreach($videos as $video)
                        <div class="col-12 col-md-6 col-lg-4 col-xl-3">
                            <div class="item-listing-container-skrn">
                                <a href="{{route('single-video', $video->id)}}"><figure class="cover-img"><img src="{{ $video->imageSrc }}" alt="{{ $video->title }}"></figure></a>
                                <div class="item-listing-text-skrn">
                                    <div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('single-video', $video->id)}}">{{ $video->title}}</a></h6>
                                        <div
                                            class="circle-rating-pro"
                                            data-value="0.86"
                                            data-animation-start-value="0.86"
                                            data-size="32"
                                            data-thickness="3"
                                            data-fill="{
							          &quot;color&quot;: &quot;#42b740&quot;
							        }"
                                            data-empty-fill="#def6de"
                                            data-reverse="true"
                                        ><span style="color:#42b740;">8.6</span></div>
                                    </div><!-- close .item-listing-text-skrn-vertical-align -->
                                </div><!-- close .item-listing-text-skrn -->
                            </div><!-- close .item-listing-container-skrn -->
                        </div><!-- close .col -->

                    @endforeach

                </div>

            </div>
            <br>
        @endif
    </main>


@endsection
