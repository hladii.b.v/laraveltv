@extends('voyager::master')
@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form"
                          class="form-edit-add"
                          action="{{ route('voyager.push.store') }}"
                          method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="panel-body">
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Title</label>
                                    <input required type="text" class="form-control" name="title" placeholder="" value="">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Text</label>
                                    <textarea required class="form-control" name="text" rows="5"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Type <span>( video / artist / category )</span></label>
                                    <input type="text" class="form-control" name="type" placeholder="" value="">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Item id <span>( video_id / artist_id / category_id )</span></label>
                                    <input type="text" class="form-control" name="item_id" placeholder="" value="">
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="name">Test push</label>
                                    <input type="checkbox" name="test" class="toggleswitch" data-on="true" data-off="false">
                                </div>

                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            @section('submit-buttons')
                                <button type="submit" class="btn btn-primary save">Send</button>
                            @stop
                            @yield('submit-buttons')
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
@stop
