@extends('layouts.app')

@section('content')
    @include('layouts.sidebar-header')

    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header-pro">
                <h2>Welcome Back</h2>
                <h6>Sign in to your account</h6>
            </div>
            <div class="modal-body-pro">

                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="form-group">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-green-pro btn-display-block">{{ __('Sign In') }}</button>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
                            <div class="col forgot-your-password">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif</div>
                        </div>
                    </div><!-- close .container-fluid -->
                </form>



            </div><!-- close .modal-body -->

            <a class="not-a-member-pro"  href="{{ route('register') }}">Not a member? <span>Join Today!</span></a>
        </div><!-- close .modal-content -->
    </div><!-- close .modal-dialog -->

@endsection
