@extends('layouts.app')

@section('content')
    <div id="content-sidebar-pro">

        <div id="content-sidebar-image">
            <img src="{{$videos->imageSrc}}" alt="{{$videos->title}}">
        </div>

        <div class="content-sidebar-section">
            <h2 class="content-sidebar-sub-header">{{$videos->title}}</h2>

        </div><!-- close .content-sidebar-section -->



    </div><!-- close #content-sidebar-pro -->


    <main id="col-main-with-sidebar">
        <div style="width: 100%">
            <iframe  id="VideoLightbox-1" src="{{$videos->src}}" width="960" height="540" frameborder="0" allow="autoplay; fullscreen" allowfullscreen><a class="movie-detail-header-play-btn afterglow" href="#VideoLightbox-1"><i class="fas fa-play"></i></a></iframe>
        </div>


        <div class="dashboard-container">


            <div class="movie-details-section">
                <h2>תיאור</h2>
                <p>{{$videos->description}}</p>
            </div><!-- close .movie-details-section -->
            @if($artists->count() >=1 )
            <div class="movie-details-section">
                <h2>משתתפים</h2>
                <div class="row">
                    @foreach($artists as $artist)
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="item-listing-container-skrn">
                            <a href="{{route('artist', $artist->id)}}"><figure class="cover-img"><img src="{{$artist->logoSrc}}" alt="{{$artist->artistName}}"></figure></a>
                            <div class="item-listing-text-skrn item-listing-movie-casting">
                                <h6><a href="{{route('artist', $artist->id)}}">{{$artist->artistName}}</a></h6>
                                <div class="movie-casting-sub-title">{{$artist->artistName}}</div>
                            </div><!-- close .item-listing-text-skrn -->
                        </div><!-- close .item-listing-container-skrn -->
                    </div><!-- close .col -->
                    @endforeach
                </div>
            </div><!-- close .movie-details-section -->
            @endif
            @if($categories->count() >=1 )
            <div class="movie-details-section">
                <h2>תכנים נוספים</h2>
                <div class="row">
                    @foreach( $categories as $cat)
                    <div class="col-12 col-md-6 col-lg-6 col-xl-3">
                        <div class="item-listing-container-skrn">
                            <a href="{{route('categories', $cat->id)}}"><figure class="cover-img"><img src="{{$cat->imageSrc}}" alt="{{$cat->title}}"></figure></a>
                            <div class="item-listing-text-skrn">
                                <div class="item-listing-text-skrn-vertical-align"><h6><a href="{{route('categories', $cat->id)}}">{{$cat->title}}</a></h6>
                                    <div
                                        class="circle-rating-pro"
                                        data-value="0.72"
                                        data-animation-start-value="0.72"
                                        data-size="32"
                                        data-thickness="3"
                                        data-fill="{
								          &quot;color&quot;: &quot;#42b740&quot;
								        }"
                                        data-empty-fill="#def6de"
                                        data-reverse="true"
                                    ><span style="color:#42b740;">7.2</span></div>
                                </div><!-- close .item-listing-text-skrn-vertical-align -->
                            </div><!-- close .item-listing-text-skrn -->
                        </div><!-- close .item-listing-container-skrn -->
                    </div><!-- close .col -->
                @endforeach
                </div><!-- close .row -->

            </div><!-- close .movie-details-section -->
                @endif

        </div><!-- close .dashboard-container -->


    </main>




@endsection
