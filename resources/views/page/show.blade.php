@extends('layouts.new.layout')
@section('content')

    <div id="content-pro">
        <div class="progression-pricing-section-background">
            <div class="container">

                <div class="row">
                    <div class="mx-auto">
                        <div style="height:70px;"></div>
                        <h2 class="short-border-bottom">{{ $page->title }}</h2>
                    </div>
                </div><!-- close .row -->

                <div style="height:25px;"></div>

                <div class="row">
                    <div class="col-md-12">
                        {!! $page->content !!}
                    </div>
                </div><!-- close .row -->
                <div class="clearfix"></div>
            </div><!-- close .container -->

        </div><!-- close .progression-pricing-section-background -->

    </div><!-- close #content-pro -->
@endsection
