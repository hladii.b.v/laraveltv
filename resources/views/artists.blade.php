@extends('layouts.app')

@section('content')




    <main id="col-main">


        <div class="clearfix"></div>

            @if($artists->count() >=1 )
            <div class="dashboard-container">
                <h2>אמנים</h2>
                <div class="row ">
                    @foreach($artists as $artist)
                        <div class="col-12 col-md-6 col-lg-6 col-xl-3">
                            <div class="item-listing-container-skrn">
                                <a href="{{route('artist', $artist->id)}}"><figure class="cover-img"><img src="{{$artist->logoSrc}}" alt="{{$artist->artistName}}"></figure></a>
                                <div class="item-listing-text-skrn item-listing-movie-casting">
                                    <h6><a href="{{route('artist', $artist->id)}}">{{$artist->artistName}}</a></h6>
                                    <div class="movie-casting-sub-title">{{$artist->artistNam}}</div>
                                </div><!-- close .item-listing-text-skrn -->
                            </div><!-- close .item-listing-container-skrn -->
                        </div><!-- close .col -->
                    @endforeach
                </div>
            </div><!-- close .movie-details-section -->
        @endif
    </main>


@endsection
