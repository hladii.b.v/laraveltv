@if($itemType == "box")
<div  onclick="location.href='{{ $route }}'" class="oc-item" data-animate="fadeInDown">
    <a href="{{ $route }}">
    <img  src="{{$item->thumbnail}}" class="boxThumb" alt="{{$item->title}}" />
    <div class="overlay">

        <div class="overlay">
            <div class="text-overlay">
                <div class="text-overlay-title">
                    <h3><a href="{{ $route }}">{{$item->title}}</a></h3>
                </div>
                <div class="text-overlay-meta">
                    <h3><a href="{{ $route }}"> {{ !empty($item->subtitle) ? $item->subtitle : "-"  }}</a></h3>
                </div>
            </div>
            <div class="on-hover">
                <a style="right:25% !important" href="{{ $route }}" class="play-icon track-list" ><i style="right:25% !important" class="icon-play"></i></a>
            </div>
        </div>

    </div>
    </a>
</div>
@endif
@if ($itemType == "circle")

<div class="oc-item" style="width:120px;">
    <div class="team">
        <div class="team-image" >
            <a href="{{ $route }}">
            <img class="circleThumb" src="{{$item->thumbnail}}" alt="{{$item->title}}">
            </a>
        </div>
        <div class="team-desc">
            <a href="{{ $route }}">
            <div class="team-title"><h5>{{$item->title}}</h5></div>
            </a>

        </div>
    </div>
</div>
@endif


