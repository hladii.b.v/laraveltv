@extends('layouts.app-home')

@section('content')
    <div id="content-pro">

        <div class="container">
            <div class="centered-headings-pro pricing-plans-headings">
                <div class="bottommargin">
                    <img src="{{asset('images/yahadtv-logo-h.png')}}" />
                </div>
                <h6> </h6>
                <h1>רוצים לקבל את אפליקציית יחד TV לאנדרואיד?</h1>
            </div>
        </div><!-- close .container -->


        <div id="pricing-plans-background-image">
            <div class="container">
                <div class="registration-steps-page-container">
                    @error('phone')

                    <div class="alert alert-success error-alert">
                        <p>המספר כבר נמצא בבסיס הנתונים</p>
                    </div>

                    @enderror
                    @if ($message = Session::get('success'))
                        <div class="registration-social-login-container">
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        </div>
                    @else
                        <form class="registration-steps-form" align="right" method="POST" action="{{ route('subscription.store') }}">
                            @csrf
                            <div class="registration-social-login-container">
                                <p><b>
                                 רוצים לקבל את אפליקציית יחד TV  לאנדרואיד? <br>
                                        הזדרזו והרשמו עכשיו וקבלו עדכון כשהאפליקציה זמינה להורדה  !<br>

                                    משתמשי אייפון שימו לב - האפליקציה לאייפון כבר זמינה להורדה בקישור הזה
                                    <a href="https://apps.apple.com/il/app/yahadtv/id1505071143">אפליקציית אייפון</a><br>

                                    </b>
                                </p>
                                <div class="form-group">
                                    <label for="phone" class="col-form-label">טלפון</label>
                                    <input required type="text" class="form-control" id="phone" name="phone"  placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="full-name" class="col-form-label">שם</label>
                                    <input type="text" class="form-control" id="full-name" name="name" placeholder="שם ומשפחה">
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-form-label">אימייל - לא חובה</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="">
                                </div>
                                <div class="form-group last-form-group-continue">
                                    <button type="submit" class="btn btn-green-pro">סיימתי!</button>

                                    <div class="clearfix"></div>
                                </div>
                                @endif


                            </div><!-- close .registration-social-login-container -->

                            <div class="registration-social-login-options" align="right">

    
                                <b>הורידו את אפליקציית יחד TV</b>

                                <p>
                                <a href="https://apps.apple.com/il/app/yahadtv/id1505071143">אפליקציית אייפון</a><br>
                                <a href="#">אפליקציית אנדרואיד- עולה אוטוטו</a><br>
                                <br><br>
                                <b>עיקבו אחרינו ברשתות החברתיות</b>
                                <br>
                                <a target="_blank" href="https://www.facebook.com/yahadtv/">פייסבוק</a><br>

                                <a target="_blank" href="http://instagram.com/yahadtv">אינסטגרם</a>


                            </div><!-- close .registration-social-login-options -->

                            <div class="clearfix"></div>

                        </form>

                </div><!-- close .registration-steps-page-container -->

            </div><!-- close .container -->
        </div><!-- close #pricing-plans-background-image -->

    </div><!-- close #content-pro -->


    <a href="#0" id="pro-scroll-top"><i class="fas fa-chevron-up"></i></a>


@endsection
