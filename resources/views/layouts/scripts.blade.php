<script src="{{ asset('js/libs/jquery-3.3.1.min.js')}}"></script><!-- jQuery -->
<script src="{{ asset('js/libs/popper.min.js')}}" defer></script><!-- Bootstrap Popper/Extras JS -->
<script src="{{ asset('js/libs/bootstrap.min.js')}}" defer></script><!-- Bootstrap Main JS -->
<!-- All JavaScript in Footer -->

<!-- Additional Plugins and JavaScript -->
<script src="{{ asset('js/navigation.js')}}" defer></script><!-- Header Navigation JS Plugin -->
<script src="{{ asset('js/jquery.flexslider-min.js')}}" defer></script><!-- FlexSlider JS Plugin -->
<script src="{{ asset('js/jquery-asRange.min.js')}}" defer></script><!-- Range Slider JS Plugin -->
<script src="{{ asset('js/circle-progress.min.js')}}" defer></script><!-- Circle Progress JS Plugin -->

<script src="{{ asset('js/script.js')}}" defer></script><!-- Custom Document Ready JS -->
<script src="{{ asset('js/script-dashboard.js')}}" defer></script><!-- Custom Document Ready for Dashboard Only JS -->


