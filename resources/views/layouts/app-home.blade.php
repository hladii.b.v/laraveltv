<!doctype html >
<html lang="{{ str_replace('_', '-', app()->getLocale()) }} " dir="rtl">
<head>
    @include('layouts.header')
</head>
<body>

<main >
    @yield('content')
</main>
@include('layouts.footer')
@include('layouts.scripts')
</body>
</html>
