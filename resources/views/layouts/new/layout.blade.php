<!DOCTYPE html>
<html dir="rtl" lang="he">
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-162914558-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-162914558-1');
</script>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="LotemX by Lotem Group" />

<!-- Stylesheets
============================================= -->
<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,400i,600,700|Montserrat:300,400,700|Caveat+Brush" rel="stylesheet" type="text/css" />

<link href="https://fonts.googleapis.com/css?family=Assistant&display=swap" rel="stylesheet">


<link rel="stylesheet" href="{{ asset('new/css/bootstrap.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/css/bootstrap-rtl.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/style.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/style-rtl.css')}}" type="text/css" />

<link rel="stylesheet" href="{{ asset('new/css/dark.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/css/dark-rtl.css')}}" type="text/css" />

<link rel="stylesheet" href="{{ asset('new/css/swiper.css')}}" type="text/css" />

<link rel="stylesheet" href="{{ asset('new/demos/music/music.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/css/app.css?d=11042020')}}" type="text/css" />


<link rel="stylesheet" href="{{ asset('new/one-page/css/et-line.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/css/animate.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/css/magnific-popup.css')}}" type="text/css" />

<link rel="stylesheet" href="{{ asset('new/demos/music/css/fonts.css')}}" type="text/css" />

<!-- Bootstrap Switch CSS -->
<link rel="stylesheet" href="{{ asset('new/css/components/bs-switches.css')}}" type="text/css" />

<link rel="stylesheet" href="{{ asset('new/css/responsive.css')}}" type="text/css" />
<meta name="viewport" content="width=device-width, initial-scale=1" />

<!-- Theme color -->
<link rel="stylesheet" href="{{ asset('new/css/colors.php?color=ed215e')}}" type="text/css" />

<!-- Audio Player Plugin CSS -->
<link rel="stylesheet" href="{{ asset('new/demos/music/css/mediaelement/mediaelementplayer.css')}}">
<meta name="keywords" content="@yield('pageKeywords')" />
<meta name="description" content="@yield('pageDescription')" />

<title>@yield('pageTitle')</title>


    <style>
        @media (max-width: 479px) {
            .swiper_wrapper:not(.force-full-screen),
            .swiper_wrapper:not(.force-full-screen):not(.canvas-slider-grid) .swiper-slide {
                height: 300px !important;
            }
        }

        .css3-spinner { background-color: #131722; }
        body,h1,h2,h3,h4,h5,p {
            font-family: 'assistant' ,'arial' !important;
        }
    </style>

<link rel="stylesheet" href="{{ asset('new/css/font-icons.css')}}" type="text/css" />
<link rel="stylesheet" href="{{ asset('new/css/font-icons-rtl.css')}}" type="text/css" />
</head>

<body class="stretched rtl bgcolor2 no-transition">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix" style="margin-bottom: 40px">

    <!-- Header
    ============================================= -->
    <header id="header"  class="transparent-header full-header" data-sticky-class="not-dark">

        <div id="header-wrap">

            <div class="container clearfix">

                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                <!-- Logo
                ============================================= -->
                <div id="logo">
                <a href="{{ route('home_new') }}" class="standard-logo" data-dark-logo="{{ asset('/images/yahadtv-logo.png') }} "><img src="{{ asset('/images/yahadtv-logo.png') }}" alt="Canvas Logo"></a>
                    <a href="{{ route('home_new') }}" class="retina-logo" data-dark-logo="demos/music/images/logo-dark@2x.png"><img src="{{ asset('/images/yahadtv-logo.png') }}" alt="Canvas Logo"></a>
                </div><!-- #logo end -->

                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu" class="clearfix not-dark">

                    <ul>
                        <li class="active"><a href="{{ route('home_new') }}"><div>ראשי</div></a></li>
                        <li><a href="{{ route('getApp') }}"><div>הורדת האפליקציה</div></a></li>
                        <li><a href="{{ route('contact') }}"><div>יצירת קשר</div></a></li>

                    </ul>

                    {{-- <!-- Top Search
                    ============================================= -->
                    <div id="top-search">
                        <a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>
                        <form action="search.html" method="get">
                            <input type="text" name="q" class="form-control" value="" placeholder="Find your Music here....">
                        </form>
                    </div><!-- #top-search end --> --}}

                </nav><!-- #primary-menu end -->

            </div>

        </div>

    </header><!-- #header end -->


    @yield('content')

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="noborder dark1" style="background-color: #111;">

        <!-- Copyrights
        ============================================= -->
        <div id="copyrights" style="color: #444;">

            <div class="container clearfix">

                <div class="col-8">
                    &copy; כל הזכויות שמורות 2020 יחדTV.
                    <a href="{{ route('page.show', 'privacy') }}">הצהרת פרטיות</a> | 
                    <a href="{{ route('page.show', 'terms-of-use') }}"> תנאי שימוש</a><br>
                     פיתוח: <a target="_blank" href="https://www.lotemx.com">LotemX קבוצת לוטם</a>

                </div>

                <div class="col-4 col_last">
                    {{-- <div class="fright clearfix">
                        <a href="#" class="social-icon si-small si-borderless si-facebook">
                            <i class="icon-facebook"></i>
                            <i class="icon-facebook"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-twitter">
                            <i class="icon-twitter"></i>
                            <i class="icon-twitter"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-gplus">
                            <i class="icon-gplus"></i>
                            <i class="icon-gplus"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-pinterest">
                            <i class="icon-pinterest"></i>
                            <i class="icon-pinterest"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-vimeo">
                            <i class="icon-vimeo"></i>
                            <i class="icon-vimeo"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-github">
                            <i class="icon-github"></i>
                            <i class="icon-github"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-yahoo">
                            <i class="icon-yahoo"></i>
                            <i class="icon-yahoo"></i>
                        </a>

                        <a href="#" class="social-icon si-small si-borderless si-linkedin">
                            <i class="icon-linkedin"></i>
                            <i class="icon-linkedin"></i>
                        </a>
                    </div> --}}
                </div>

            </div>

        </div><!-- #copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->


<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up" style="bottom: 70px;"></div>

<!-- External JavaScripts
============================================= -->
<script src="{{ asset('new/js/jquery.js')}}"></script>
<script src="{{ asset('new/js/plugins.js')}}"></script>

<!-- Audio player Plugin
============================================= -->
<script src="{{ asset('new/demos/music/js/mediaelement/mediaelement-and-player.js')}}"></script>

<!-- Footer Scripts
============================================= -->
<script src="{{ asset('new/js/functions.js')}}"></script>

<script>






</script>

</body>
</html>
