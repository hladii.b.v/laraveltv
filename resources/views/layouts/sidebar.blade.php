<nav id="sidebar-nav" style="font-family: 'Montserrat', sans-serif;"><!-- Add class="sticky-sidebar-js" for auto-height sidebar -->
    <ul id="vertical-sidebar-nav" class="sf-menu">
        <li class="normal-item-pro {{ Request::is( 'beta' ) ? 'current-menu-item' : '' }}">
            <a href="{{route('home')}}">
                <span class="icon-Old-TV font-icon"></span>
                ערוץ LIVE
            </a>
        </li>
        <li class="normal-item-pro {{ Request::is( 'categories' ) ? 'current-menu-item' : '' }}">
            <a href="{{route('categories')}}">
                <span class="icon-Reel  font-icon"></span>
                סדרות
            </a>
        </li>
        <li class="normal-item-pro {{ Request::is( 'videos' ) ? 'current-menu-item' : '' }}">
            <a href="{{route('all-videos')}}">
                <span class="icon-Movie  font-icon"></span>
                הופעות והצגות
            </a>
        </li>
        <li class="normal-item-pro {{ Request::is( 'artists' ) ? 'current-menu-item' : '' }}">
            <a href="{{route('all-artists')}}">
                <span class="icon-Movie-Ticket  font-icon"></span>
                כל הכוכבים
            </a>
        </li>
        <li class="normal-item-pro">
            <a href="#">
                <span class="icon-Clock  font-icon"></span>
                הצטרפו אלינו
            </a>
        </li>

    </ul>
    <div class="clearfix"></div>
</nav>
