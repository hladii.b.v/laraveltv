<header id="videohead-pro" class="sticky-header">
    <div id="video-logo-background"><a href="{{ '/'}}"><img src="{{ asset('images/yahadtv-logo.png')}}" alt="Logo" class="registration-logo"></a></div>

    <div id="video-search-header">
        {{-- <div id="search-icon-more"></div>
        <input type="text" placeholder="הקלידו על מנת לחפש תכנים" aria-label="Search"> --}}

    </div><!-- close .video-search-header -->


    <nav id="site-navigation-pro">
       {{-- <ul class="sf-menu">
            <li class="normal-item-pro">
                <a href="#">Home</a>
            </li>
            <li class="normal-item-pro">
                <a href="#">Home</a>
                <!-- Sub-Menu Example >
                <ul class="sub-menu">
                    <li class="normal-item-pro">
                        <a href="#!">Sub-menu item 1</a>
                    </li>
                    <li class="normal-item-pro">
                        <a href="#!">Sub-menu item 2</a>
                    </li>
                    <li class="normal-item-pro">
                        <a href="#!">Sub-menu item 3</a>
                    </li>
                </ul>
                < End Sub-Menu example -->
            </li>
            <li class="normal-item-pro">
                <a href="#">Home</a>
            </li>
            <li class="normal-item-pro current-menu-item">
                <a href="#">Home</a>
            </li>
        </ul>--}}
    </nav>

    <!-- Right Side Of Navbar -->
        <!-- Authentication Links -->


        @guest

        @if (Route::has('register'))
               {{-- <a    href="{{ route('register') }}" class="btn btn-header-pro btn-green-pro noselect">{{ __('Register') }}</a>

                <a class="btn btn-header-pro noselect" href="{{ route('login') }}">{{ __('Login') }}</a>--}}
        @endif

        @else
            <div id="header-user-profile">
                {{--  <div id="header-user-profile-click" class="noselect">
                     <img src="{{ asset('images/user-profile.jpg')}}" alt="Suzie">
                     <div id="header-username">{{ Auth::user()->name }}</div><i class="fas fa-angle-down"></i>
                 </div><!-- close #header-user-profile-click -->
                 <div id="header-user-profile-menu">
                    <ul>
                         <li><a href="/admin"><span class="icon-User"></span>My Profile</a></li>
                         --}}{{--<li><a href="dashboard-favorites.html"><span class="icon-Favorite-Window"></span>My Favorites</a></li>
                         <li><a href="dashboard-account.html"><span class="icon-Gears"></span>Account Details</a></li>
                         <li><a href="#!"><span class="icon-Life-Safer"></span>Help/Support</a></li>--}}{{--
                         <li><a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();   document.getElementById('logout-form').submit();">
                                 <span class="icon-Power-3"></span>{{ __('Logout') }}  </a>

                                     <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                         @csrf
                                     </form>
                         </li>
                     </ul>--}}
                </div><!-- close #header-user-profile-menu -->
            </div><!-- close #header-user-profile -->
        @endguest

    <div class="clearfix"></div>



</header>
