<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="LotemX by Lotem Group" />
<meta name="keywords" content="@yield('pageKeywords')" />
<meta name="description" content="@yield('pageDescription')" />
<title>@yield('pageTitle')</title>
<!-- Scripts -->


<!-- Fonts -->
<link rel="dns-prefetch" href="//fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

<!-- Styles -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Gentium+Basic">

<link rel="stylesheet" href="{{ asset('css/bootstrap/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{ asset('css/style.css')}}">
<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,700%7CMontserrat:300,400,600,700">

<link rel="stylesheet" href="{{ asset('icons/fontawesome/css/fontawesome-all.min.css')}}"><!-- FontAwesome Icons -->
<link rel="stylesheet" href="{{ asset('icons/Iconsmind__Ultimate_Pack/Line%20icons/styles.min.css')}}"><!-- iconsmind.com Icons -->
<link href="https://fonts.googleapis.com/css?family=Assistant&display=swap" rel="stylesheet">