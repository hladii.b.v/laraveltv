<footer id="footer-pro">
    <div class="container">
        <div class="row">
            <div class="col-md">
                <div class="copyright-text-pro">
                    &copy; כל הזכויות שמורות 2020 יחדTV.
                    <a href="{{ route('page.show', 'privacy') }}">הצהרת פרטיות</a> | 
                    <a href="{{ route('page.show', 'terms-of-use') }}"> תנאי שימוש</a>
                </div>

            </div> 
        </div><!-- close .row -->
    </div><!-- close .container -->
</footer>
