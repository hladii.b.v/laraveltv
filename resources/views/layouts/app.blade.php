<!doctype html >
<html lang="{{ str_replace('_', '-', app()->getLocale()) }} " dir="rtl">
<head>
    @include('layouts.header')
</head>
<body>


    @include('layouts.sidebar-header')
    @include('layouts.sidebar')
    @yield('content')

    @include('layouts.footer')
    @include('layouts.scripts')
</body>
</html>
