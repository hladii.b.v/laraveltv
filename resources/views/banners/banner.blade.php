@php
$class = isset($class) ? $class : "";
@endphp
<!-- AD START - BANNER | Position: {{$position}} | Width: {{$width}} | Height: {{$height}} | Class: {{ $class }} -->
@foreach(App\Ad::get($position,1) as $ad)
<a href="{{ $ad->trackedLink }}" target="_blank">
    <img src="{{ asset("storage/".$ad->image) }}" class="{{ $class }}" alt="yahadtv banner Position: {{$position}}" style='width:{{ $width}};height:{{ $height }};object-fit:cover' />
</a>
@endforeach
<!-- AD END ({{$position}}) -->    