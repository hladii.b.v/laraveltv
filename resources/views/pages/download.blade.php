@extends('layouts.new.layout')

@section('content')


<div class="content-wrap nopadding">

    <div class="container clearfix" style="z-index: 7;">


<div class="row topmargin dark1 clearfix">
    <div class="col-lg-12 col-md-5 offset-md-0 ohidden">
        <div class="row clearfix">
            <div class="col-lg-6 order-lg-12">
                <div class="d-block d-md-none d-lg-none d-xl-block topmargin clear"></div>
                <h3>קבלו את האפליקצייה של יחד TV</h3>
                <p>האפליקציה זמינה להורדה בחינם בחנויות האפליקציות.</p>
                <a href="https://apps.apple.com/il/app/yahadtv/id1505071143" class="button button-small button-rounded button-desc noleftmargin clearfix"><i class="icon-apple"></i><div>App Store<span>בחינם, זמינה להורדה</span></div></a>
            <a href="https://play.google.com/store/apps/details?id=com.yahadtv&hl=he_IW" class="button button-small button-rounded button-desc noleftmargin clearfix" style="margin-top: 10px"><i class="icon-android"></i><div>Google Play<span>בחינם, זמינה להורדה</span></div></a>
            </div>
            <div class="d-block d-md-block d-lg-none topmargin clear"></div>
        
        </div>
    </div>
</div>
    </div>
</div>

<script>


function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    console.log(userAgent);
      // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
        return "Windows Phone";
    }

    if (/android/i.test(userAgent)) {
        location.href = "https://play.google.com/store/apps/details?id=com.yahadtv&hl=he_IW";
        return "Android";


    }  

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        location.href = "https://apps.apple.com/il/app/yahadtv/id1505071143";
        return "iOS";
    }  
}

getMobileOperatingSystem();
</script>
@endsection