@extends('layouts.new.layout')

@section('content')


<div class="content-wrap nopadding">

    <div class="container clearfix" style="z-index: 7;">

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="row">
						<!-- Google Map
						============================================= -->
						<div class="col-lg-6 bottommargin">

		<!-- Postcontent
					============================================= -->
					<div class="postcontent nobottommargin">

                        @if ($message = Session::get('success'))
                        <div class="registration-social-login-container">
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        </div>
                    @else

                        <h1>צרו עמנו קשר</h1>

						<div class="form-widget">
							<div class="form-result"></div>


							<form class="nobottommargin" name="contactform" action="{{ route('contact.store') }}" method="post">
                                @csrf
								<div class="form-process"></div>

								<div class="col_one_third">
									<label for="template-contactname">שם <small>*</small></label>
									<input type="text" id="name" name="name" value="" required class="sm-form-control required" />
								</div>

								<div class="col_one_third">
									<label for="email">אימייל <small>*</small></label>
									<input type="email" id="email" name="email" required value=""  class="required email sm-form-control" />
								</div>

								<div class="col_one_third col_last">
									<label for="phone">טלפון</label>
									<input type="text" id="phone" name="phone" value="" required class="sm-form-control" />
								</div>

								<div class="clear"></div>

								<div class="col_two_third">
									<label for="subject">נושא <small>*</small></label>
									<input type="text" id="subject" name="subject" value="" required class="required sm-form-control" />
								</div>


								<div class="clear"></div>

								<div class="col_full">
									<label for="message">תוכן ההודעה <small>*</small></label>
									<textarea class="required sm-form-control" id="message" required name="message" rows="6" cols="30"></textarea>
								</div>

                                <div class="col_full hidden">
									<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
								</div>

								<div class="col_full">
									<button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">שליחה</button>
								</div>


							</form>
						</div>
                        @endif
					</div><!-- .postcontent end -->



					</div>

				</div>

			</div>

		</section><!-- #content end -->



    </div>
</div>


@endsection