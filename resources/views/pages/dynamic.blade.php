@extends('layouts.new.layout')
@section('pageTitle', $meta['pageTitle'])
@section('pageDescription', $meta['pageDescription'])
@section('pageKeywords', $meta['pageKeywords'] )
@section('content')



@if ($type == "home")

    <!-- Slider
    ============================================= -->
    <section id="slider" class=" dark slider-element clearfix swiper_wrapper" style="background: #131722; height: 800px" data-effect="fade" data-loop="true" data-speed="1000">
        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <div class="swiper-slide dark1" style="background: url('https://www.yahadtv.co.il/storage/settings/March2020/8pxSwKb2CJ0QGbUMhBra.jpeg') no-repeat bottom center; background-size: cover">
                    <div class="container clearfix">
                        <div class="slider-caption">
                            <h2 data-animate="fadeInUp" data-delay="0" class=" nott rtli"><b> ברוכים הבאים ליחד TV!</b></h2>
                            <p class="d-none d-md-block rtli">{{$data->description }}</p>
                            <a  href="#myModal1" data-lightbox="inline" data-animate="fadeInUp" data-delay="200" class="button button-rounded t400 ls1 track-list clearfix"  style="margin-top: 15px"><i class="icon-play"></i> לחצו לצפייה</a>
                            <a  href="/get"  data-animate="fadeInUp" data-delay="200" class="button button-rounded t400 ls1 track-list clearfix"  style="margin-top: 15px"><i class="icon-apple"></i><i class="icon-android"></i> הורידו את האפליקציה</a>

                        </div>
                    </div>
                </div>
                
                
            </div>
            <a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

        </div>


    </section>


@else 

@if ($type == "video")

        <div class="section nobg notopmargin noborder nobottompadding">
            <div class="container clearfix">



                <div class="col_full  ">

                        
                @if (!empty($data->videoSrc))

                    <div class="embed-container">
                    <iframe src="{{ $data->videoSrc }}?autoplay=1&loop=1&autopause=0" allow="autoplay; fullscreen" style="width:100%; height:100%" frameborder="0" ></iframe>
                    </div>
                @else
                <img src="{{ $data->coverImage }}" style="max-height: 200px" alt="{{ $data->title }}" data-animate="fadeInRight" class="fadeInRight animated">

                @endif


                </div>

                <div class="col_full nobottommargin col_last">

                    <div style="padding-top: 0px;">

                        <h2>{{ $data->title }}</h2>
                        <span>{{$data->description }}</span>
                    </div>

                </div>


            </div>
        </div>

@else
        
        <div class="section nobg notopmargin noborder ">
            <div class="container clearfix">

                <div class="col_half bottommargin-sm center">

                    
                @if (!empty($data->videoSrc))
                    <iframe src="{{ $data->videoSrc }}" width="100%" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

                @else
                <img src="{{ $data->coverImage }}" style="max-height: 200px" alt="{{ $data->title }}" data-animate="fadeInRight" class="fadeInRight animated">

                @endif


                </div>

                <div class="col_half nobottommargin col_last">

                    <div  style="padding-top: 0px;">

                        <h2>{{ $data->title }}</h2>
                        <span>{{$data->description }}</span>

                    </div>

                </div>


            </div>
        </div>
@endif

@endif


    <!-- Content
    ============================================= -->
    <section id="content" class="bgcolor2" style="overflow: visible;">


    
@if (!empty($data->videoSrc))
        <!-- Modal -->
        <div class="modal1 mfp-hide" id="myModal1">
            <div class="block divcenter" style="background-color: #FFF; max-width: 600px;">
                <div class="center clearfix">
                <iframe src="{{ $data->videoSrc }}" width="400" height="300" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    <div class="section center nomargin" style="padding: 0px;">
                        <a href="#" class="button" onClick="$.magnificPopup.close();return false;">סגירה</a>
                    </div>
                </div>
            </div>
        </div>
@endif
   




        <div class="content-wrap nopadding">

            <div class="container clearfix" style="z-index: 7;">


                @foreach($data->sections as $key=>$section)


                @if(!empty($section->sectionType) )
                    @if (is_array($section->items) && sizeof($section->items))
                    <div class="divider divider-center"><i class="icon-heart"></i></div>
                    @endif
                    
                    @if($section->sectionType == "posts")

                        @if (is_array($section->items) && sizeof($section->items))
                        <div class="heading-block bottommargin-sm  noborder dark1">
                            <h2>{{ $section->title }}</h2>
                        </div>

                        <div id="posts{{ $key }}"  class="post-grid grid-container clearfix" data-layout="fitRows">

                                @foreach($section->items as $item)
                                    @php
                                        $route = "#";
                                        if ($item->type && $item->id)
                                            $route = route('dynamic',["type"=>$item->type , "id"=>$item->id]);
                        
                                    @endphp
                                    <div class="entry y_entry clearfix">
                                        <div class="entry-image y_entry-image">
                                            <a href="{{ $route }}"><img class="image_fade y_post_entry_img"  src="{{ $item->cover }}" alt="{{$item->title}}"></a>
                                        </div>
                                        <div class="entry-title">
                                            <h2><a href="{{ $route }}">{{$item->title}}</a></h2>
                                        </div>
                                        <ul class="entry-meta clearfix">
                                        <li>{{ $item->subtitle }}</li>
                                        </ul>
                                    </div>
                                
                                @endforeach
                            </div>
                            @endif
                    @endif

                    @if($section->sectionType == "float")

                        @if (is_array($section->items) && sizeof($section->items))
                            <div class="heading-block bottommargin-sm  noborder dark1">
                                <h2>{{ $section->title }}</h2>
                            </div>

                            
                            @foreach($section->items as $item)
                            @php
                                $route = "#";
                                if ($item->type && $item->id)
                                    $route = route('dynamic',["type"=>$item->type , "id"=>$item->id]);
                
                            @endphp

                            <div style="float:right;margin-left:20px;">
                            @include('partials.thumbnail',["itemType"=>$section->itemType, "item"=>$item,"route"=>$route] )
                            </div>

    
                        
                        @endforeach
                            <div class="clear"></div>
                            @endif
                    @endif

                    @if($section->sectionType == "banners")

                    @if (is_array($section->items) && sizeof($section->items))
                        @if($section->title)
                        <div class="heading-block bottommargin-sm  noborder dark1">
                            <h2>{{ $section->title }}</h2>
                        </div>
                        @endif
                        
                        <div class="clearfix topmargin">

                            <div class="row clearfix">  
                        @foreach($section->items as $item)
                            @php
                                $route = "#";
                                if ($item->type && $item->id)
                                    $route = route('dynamic',["type"=>$item->type , "id"=>$item->id]);
                
                            @endphp

                                <div class="col">
                                    <a href="{{ $item->url }}" target="_blank">
                                        <img src="{{ $item->thumbnail }}" alt="yahadtv banner" style='width:100%;height: auto ;object-fit:cover' />
                                    </a>
                                </div>
                    
                    @endforeach
                    </div>
                </div>
                        <div class="clear"></div>
                        @endif
                @endif


                @else 


                        @if (is_array($section->items) && sizeof($section->items))
                        <!-- Carousel -->
                        <div class="heading-block topmargin-sm noborder dark1">
                            <h2>{{ $section->title }}</h2>
                        </div>
                        <div id="oc-carousel-{{ $key }}" class="oc-populer-carousel owl-carousel image-carousel carousel-widget" data-margin="10" data-nav="true" data-pagi="true" data-items-xs="3" data-items-sm="3" data-items-md="4" data-items-lg="7" data-items-xl="7">


                            @foreach($section->items as $item)
                                @php
                                    $route = "#";
                                    if ($item->type && $item->id)
                                        $route = route('dynamic',["type"=>$item->type , "id"=>$item->id]);
                    
                                @endphp
    
                                <!-- Carousel Items
                                ============================================= -->
                                @include('partials.thumbnail',["itemType"=>$section->itemType, "item"=>$item,"route"=>$route] )

                               
                                
                             
                            @endforeach
                        </div>

                        @endif
          
                   @endif


            @endforeach
    


                <div class="clear"></div>

                <div class="row topmargin dark1 clearfix">
                    <div class="col-lg-12 col-md-5 offset-md-0 ohidden">
                        <div class="row clearfix">
                            <div class="col-lg-6 order-lg-12">
                                <div class="d-block d-md-none d-lg-none d-xl-block topmargin clear"></div>
                                <h3>קבלו את האפליקצייה של יחד TV</h3>
                                <p>האפליקציה זמינה להורדה בחינם בחנויות האפליקציות.</p>
                                <a href="https://apps.apple.com/il/app/yahadtv/id1505071143" class="button button-small button-rounded button-desc noleftmargin clearfix"><i class="icon-apple"></i><div>App Store<span>בחינם, זמינה להורדה</span></div></a>
                            <a href="https://play.google.com/store/apps/details?id=com.yahadtv&hl=he_IW" class="button button-small button-rounded button-desc noleftmargin clearfix" style="margin-top: 10px"><i class="icon-android"></i><div>Google Play<span>בחינם, זמינה להורדה</span></div></a>
                            </div>
                            <div class="d-block d-md-block d-lg-none topmargin clear"></div>
                        
                        </div>
                    </div>
                </div>



            </div>

        </div>

    </section><!-- #content end -->


@if ($isMobile)
    <div class="modal-on-load enable-cookie" data-delay="2000" data-target="#myModal1"></div>

					<!-- Modal -->
					<div class="modal1 mfp-hide" id="myModal1">
						<div class="block divcenter" style="background-color: #FFF; max-width: 700px; direction:rtl">
							<div class="center" style="padding: 50px;">
								<h3>הורידו את אפליקציית יחד TV - עכשיו   בחינם </h3>
								<h4 class="nobottommargin">
                                    אפליקציית הוידאו של יחד TV כוללת<br>
                                   <i class="icon-line-heart"></i> שידורים חיים של הכוכבים האהובים<br>
                                   <i class="icon-line-heart"></i>  תכניות 
                                   <i class="icon-line-heart"></i> הופעות
                                   <i class="icon-line-heart"></i> ועוד!
                                    <br><br>
כולם כבר באפליקציה! למה אתם מחכים?
                                </h4>
							</div>
                            <div class="section center nomargin" style="padding: 0px;">
                                <a href="/get" class="button">לחצו להורדת האפליקציה בחינם!</a>
                                <br><br>
								<a href="#" class="" onClick="$.magnificPopup.close();return false;">אל תציגו לי שוב</a>
							</div>
						</div>
					</div>

@endif
@endsection
