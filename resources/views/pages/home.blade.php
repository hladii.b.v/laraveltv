@extends('layouts.new.layout')
@section('pageTitle', $meta['pageTitle'])
@section('pageDescription', $meta['pageDescription'])
@section('pageKeywords', $meta['pageKeywords'] )
@section('content')



    <!-- Slider
    ============================================= -->
    <section id="slider" class=" dark slider-element clearfix swiper_wrapper" style="background: #131722; height: 800px" data-effect="fade" data-loop="true" data-speed="1000">
        <div class="swiper-container swiper-parent">
            <div class="swiper-wrapper">
                <div class="swiper-slide dark1" style="background: url('https://www.yahadtv.co.il/storage/settings/March2020/8pxSwKb2CJ0QGbUMhBra.jpeg') no-repeat bottom center; background-size: cover">
                    <div class="container clearfix">
                        <div class="slider-caption">
                            <h2 class="font-primary nott rtli">ברוכים הבאים ליחד TV!</h2>
                            <p class="d-none d-md-block rtli">אמני ישראל ביחד למען ילדי ישראל.</p>
                            <a data-toggle="modal" data-target="#myModal"  class="button button-rounded t400 ls1 track-list clearfix"  style="margin-top: 15px"><i class="icon-play"></i> לחצו לצפייה</a>
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>



        <!-- The Modal -->
            <div id="myModal" class="modal">

                <!-- Modal content -->
                <div class="modal-content">
                    <span class="close">&times;</span>
                    <iframe  id="VideoLightbox-1" src="{{ setting('site.homeVideoSrc') }}" width="800" height="400" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>

                </div>

            </div>


    </section>

    <!-- Content
    ============================================= -->
    <section id="content" class="bgcolor2" style="overflow: visible;">

        <div class="content-wrap nopadding">

            <div class="container clearfix" style="z-index: 7;">


                @foreach($data as $catName=>$items)


                <!-- Carousel -->
                <div class="heading-block bottommargin-sm topmargin noborder dark1">
                    <h3>{{ $catName }}</h3>
                </div>
                        <div id="oc-carousel-{{ sha1($catName) }}" class="owl-carousel image-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="6" data-items-xl="6">

                        @if (is_array($items) && sizeof($items))
                            @foreach($items as $item)
                                @php
                                    $route = "#";
                                    if ($item->type == "video" ) $route = route('single-video', $item->id);
                                    if ($item->type == "category" ) $route = route('categories', $item->id);
                                    if ($item->type == "artist" ) $route = route('artist', $item->id);
    
                                @endphp
    
                                <!-- Carousel Items
                                ============================================= -->
                                
                                <div class="oc-item" data-animate="fadeInDown">
                                    <a href="{{ $route }}"><img src="{{$item->thumbnail}}" alt="{{$item->title}}"></a>
                                    <div class="overlay">
                                        <div class="text-overlay">
                                            <div class="text-overlay-title">
                                                <h3><a href="{{ $route }}">{{$item->title}}</a></h3>
                                            </div>
                                            <div class="text-overlay-meta">
                                                <a href="#"></a>
                                            </div>
                                        </div>
                                        <div class="on-hover">
                                            <a href="{{ $route }}" class="ellipsis-icon"><i class="icon-line-ellipsis"></i></a>
                                        </div>
                                    </div>
                                </div>


    
                            @endforeach
                        @endif
          
                   </div>
            @endforeach
    



                <!-- Tabs
                ============================================= -->
                <div class="tabs tabs-bb tabs-music-demo tabs-responsive dark1 topmargin-lg clearfix" id="album-tab" data-accordion-style="accordion-bg">

                    <ul class="tab-nav nobottomborder clearfix">
                        <li><a href="#tabs-1">Featured</a></li>
                        <li><a href="#tabs-2">Recommended</a></li>
                        <li><a href="#tabs-3">Popular</a></li>
                        <li><a href="#tabs-4">Latest</a></li>
                    </ul>

                    <!-- Tabs Container
                    ============================================= -->
                    <div class="tab-container music-tabs-albums">

                        <!-- Tab Content 1
                        ============================================= -->
                        <div class="tab-content clearfix" id="tabs-1">
                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <img src="demos/music/images/album-artworks/1/1.jpg" alt="">
                                    <div class="overlay">
                                        <div class="text-overlay">
                                            <span class="font-primary">Justin Bieber</span>
                                            <div class="text-overlay-title">
                                                <h2><a href="#" class="font-secondary">Collections of My Worlds</a></h2>
                                            </div>
                                            <a href="#" class="button button-circle button-small t400 ls1 noleftmargin track-list clearfix" data-track="demos/music/tracks/fallin-extended-mix.mp3" data-poster="demos/music/tracks/poster-images/fallin-extended-mix.jpg" data-title="Fallin Extended Mix" data-singer="Justin" style="width: auto; display: inline-block; margin-top: 15px"><i class="icon-play"></i>Play Now</a>
                                            <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                        </div>
                                        <div class="on-hover">
                                            <a href="#"><i class="icon-line-heart"></i></a>
                                            <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mx-0 px-0 mt-3 mt-md-0 col-md-6">
                                    <div class="col-6">
                                        <img src="demos/music/images/album-artworks/1/2.jpg" alt="Locked Steel Gate">
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Nicki Minaj</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Pills n Positions</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#"><i class="icon-line-ellipsis"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <img src="demos/music/images/album-artworks/1/3.jpg" alt="Mac Sunglasses">
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Rihanna</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Latest Collections of Rihanna</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#"><i class="icon-line-ellipsis"></i></a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-3">
                                        <img src="demos/music/images/album-artworks/1/4.jpg" alt="">
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">David Guetta</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Collections of David Guetta</a></h2>
                                                </div>
                                                <a href="#" class="button button-circle button-small t400 ls1 noleftmargin track-list clearfix" data-track="demos/music/tracks/something-about-love.mp3" data-poster="demos/music/tracks/poster-images/something-about-love.jpg" data-title="Something About Love" data-singer="Sian" style="width: auto;display: inline-block; margin-top: 15px"><i class="icon-play"></i>Play Now</a>
                                                <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#"><i class="icon-line-ellipsis"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Tab Content 2
                        ============================================= -->
                        <div class="tab-content clearfix" id="tabs-2">
                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <img src="demos/music/images/album-artworks/2/1.jpg" alt="">
                                    <div class="overlay">
                                        <div class="text-overlay">
                                            <span class="font-primary">Katy Perry</span>
                                            <div class="text-overlay-title">
                                                <h2><a href="#" class="font-secondary">Collections of Katy Perry</a></h2>
                                            </div>
                                            <a href="#" class="button button-circle button-small t400 ls1 noleftmargin clearfix" style="width: auto;display: inline-block; margin-top: 15px">Play Now</a>
                                            <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                        </div>
                                        <div class="on-hover">
                                            <a href="#"><i class="icon-line-heart"></i></a>
                                            <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mx-0 px-0 mt-3 mt-md-0 col-md-6">
                                    <div class="col-6">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/2/2.jpg" alt="Locked Steel Gate">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Bruno Mars</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Just The Way You Are</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/2/3.jpg" alt="Mac Sunglasses">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Jennifer Lopez</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Ain’t Your Mama</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-3">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/2/4.jpg" alt="">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Taylor Swift</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Shake It Off</a></h2>
                                                </div>
                                                <a href="#" class="button button-circle button-small t400 ls1 noleftmargin clearfix" style="width: auto;display: inline-block; margin-top: 15px">Play Now</a>
                                                <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                    <div class="dropdown-divider"></div>
                                                    <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- Tab Content 3
                        ============================================= -->
                        <div class="tab-content clearfix" id="tabs-3">
                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <img src="demos/music/images/album-artworks/1/1.jpg" alt="">
                                    <div class="overlay">
                                        <div class="text-overlay">
                                            <span class="font-primary">Justin Bieber</span>
                                            <div class="text-overlay-title">
                                                <h2><a href="#" class="font-secondary">Collections of My Worlds</a></h2>
                                            </div>
                                            <a href="#" class="button button-circle button-small t400 ls1 noleftmargin clearfix" style="width: auto;display: inline-block; margin-top: 15px">Play Now</a>
                                            <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                        </div>
                                        <div class="on-hover">
                                            <a href="#"><i class="icon-line-heart"></i></a>
                                            <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mx-0 px-0 mt-3 mt-md-0 col-md-6">
                                    <div class="col-6">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/1/2.jpg" alt="Locked Steel Gate">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Nicki Minaj</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Pills n Positions</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/1/3.jpg" alt="Mac Sunglasses">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Rihanna</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Latest Collections of Rihanna</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-3">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/1/4.jpg" alt="">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">David Guetta</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Collections of David Guetta</a></h2>
                                                </div>
                                                <a href="#" class="button button-circle button-small t400 ls1 noleftmargin clearfix" style="width: auto;display: inline-block; margin-top: 15px">Play Now</a>
                                                <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <!-- Tab Content 4
                        ============================================= -->
                        <div class="tab-content clearfix" id="tabs-4">
                            <div class="row clearfix">

                                <div class="col-md-6">
                                    <img src="demos/music/images/album-artworks/2/1.jpg" alt="">
                                    <div class="overlay">
                                        <div class="text-overlay">
                                            <span class="font-primary">Katy Perry</span>
                                            <div class="text-overlay-title">
                                                <h2><a href="#" class="font-secondary">Collections of Katy Perry</a></h2>
                                            </div>
                                            <a href="#" class="button button-circle button-small t400 ls1 noleftmargin clearfix" style="width: auto;display: inline-block; margin-top: 15px">Play Now</a>
                                            <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                        </div>
                                        <div class="on-hover">
                                            <a href="#"><i class="icon-line-heart"></i></a>
                                            <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row mx-0 px-0 mt-3 mt-md-0 col-md-6">
                                    <div class="col-6">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/2/2.jpg" alt="Locked Steel Gate">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Bruno Mars</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Just The Way You Are</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/2/3.jpg" alt="Mac Sunglasses">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Jennifer Lopez</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Ain’t Your Mama</a></h2>
                                                </div>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 mt-3">
                                        <a href="#">
                                            <img src="demos/music/images/album-artworks/2/4.jpg" alt="">
                                        </a>
                                        <div class="overlay">
                                            <div class="text-overlay">
                                                <span class="font-primary">Taylor Swift</span>
                                                <div class="text-overlay-title">
                                                    <h2><a href="#" class="font-secondary">Shake It Off</a></h2>
                                                </div>
                                                <a href="#" class="button button-circle button-small t400 ls1 noleftmargin clearfix" style="width: auto;display: inline-block; margin-top: 15px">Play Now</a>
                                                <a href="#" class="button button-circle button-small button-border button-white button-light t400 ls1 clearfix" style="width: auto;display: inline-block; margin-top: 15px">View All</a>
                                            </div>
                                            <div class="on-hover">
                                                <a href="#"><i class="icon-line-heart"></i></a>
                                                <a href="#" data-toggle="dropdown"><i class="icon-line-ellipsis"></i></a>
                                                <ul class="dropdown-menu">
                                                    <li>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-plus"></span> Add to Queue</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-music"></span> Add to Playlist</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-cloud-download"></span> Download Offline</a>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-heart"></span> Love</a>
                                                        <div class="dropdown-divider"></div>
                                                        <a class="dropdown-item" href="#"><span class="icon-line-share"></span> Share</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

                <!-- Top Charts
                ============================================= -->
                <div class="heading-block bottommargin-sm topmargin noborder dark1">
                    <h3>Top Charts</h3>
                    <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                </div>


                <div id="top-charts" class="portfolio grid-container portfolio-6 dark1 clearfix">

                    <article class="portfolio-item" data-animate="fadeIn">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/1.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Am I that easy</a></h3>
                            <span><a href="#">Jim Reeves</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="100">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/2.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Bachelor boy</a></h3>
                            <span><a href="#">Arcade Fire</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="200">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/3.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">I don’t like it, I love it </a></h3>
                            <span><a href="#">Flo Rida ft. Robin Thicke</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="300">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/4.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Into the night</a></h3>
                            <span><a href="#">Benny Mardones</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="400">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/5.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Missing you </a></h3>
                            <span><a href="#">John Waite</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="500">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/6.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Only you</a></h3>
                            <span><a href="#">Ayo</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="50">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/7.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Over and over again </a></h3>
                            <span><a href="#">Nathan Sykes</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="150">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/8.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Rolling in the deep</a></h3>
                            <span><a href="#">Adele</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="250">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/9.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Story of my life</a></h3>
                            <span><a href="#">One Direction</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="350">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/10.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Take me to church</a></h3>
                            <span><a href="#">Hozier</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="450">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/11.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Want to want me</a></h3>
                            <span><a href="#">Jason Derulo</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="550">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/12.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">What's going on</a></h3>
                            <span><a href="#">Marvin Gaye</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="200">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/13.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Whispering hope</a></h3>
                            <span><a href="#">Jim Reeves</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="300">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/14.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">With or without you</a></h3>
                            <span><a href="#">U2</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="400">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/15.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Seasons in the sun </a></h3>
                            <span><a href="#">Westlife</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="500">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/16.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Red River Valley</a></h3>
                            <span><a href="#">Marty Robbins</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="600">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/17.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">My happiness</a></h3>
                            <span><a href="#">Connie Francis</a></span>
                        </div>
                    </article>

                    <article class="portfolio-item" data-animate="fadeIn" data-delay="700">
                        <div class="portfolio-image">
                            <a href="#">
                                <img src="demos/music/images/charts/18.jpg" alt="">
                            </a>
                            <div class="portfolio-overlay">
                                <a href="#" class="left-icon"><i class="icon-line-folder"></i></a>
                                <a href="#" class="right-icon"><i class="icon-line-share"></i></a>
                            </div>
                        </div>
                        <div class="portfolio-desc">
                            <h3><a href="#">Good kisser</a></h3>
                            <span><a href="#">Usher</a></span>
                        </div>
                    </article>

                </div>

                <!-- Singers
                ============================================= -->
                <div class="section nobg topmargin-sm nobottommargin clearfix">
                    <div class="heading-block center noborder dark1">
                        <h2 class="nott">Top Singers</h2>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>
                    </div>
                    <div class="singer-wrap clearfix">
                        <a href="#" class="singer-bb-image big" style="background-image: url('demos/music/images/singers/david.jpg');"><span>David Guetta</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/jennifer-lopez.jpg'); top: 0px; left: 3%;"><span>Jennifer Lopez</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/selena-gomez.jpg'); top: 43%; margin-left: -49%;"><span>Selena Gomez</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/bruno-mars.jpg'); top: 34%; margin-left: -38%;"><span>Bruno Mars</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/lady-gaga.jpg'); top: 0; margin-left: -33%;"><span>Lady Gaga</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/ariana-grande.jpg'); top: 0; margin-left: -20%;"><span>Ariana Grande</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/chris-brown.jpg'); top: auto; bottom: 0%; margin-left: -42%;"><span>Chris Brown</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/katy-perry.jpg'); top: 34%; margin-left: -27%;"><span>Katy Perry</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/flo-rida.jpg'); top: auto; bottom: 0; margin-left: -31%;"><span>Flo Rida</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/martin-garrix.jpg'); top: auto; bottom: 5%; margin-left: -21%;"><span>Martin Garrix</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/nicki-minaj.jpg'); top: 0px; left: auto; right: 4%;"><span>Nicki Minaj</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/justin-bieber.jpg'); top: 44%; margin-left: 41%;"><span>Justin Bieber</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/rihanna.jpg'); top: 32%; margin-left: 30%;"><span>Rihanna</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/ed-sheeran.jpg'); top: auto; bottom: 2%; margin-left: 34%;"><span>Ed Sheeran</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/u2.jpg'); top: 1%; margin-left: 26%;"><span>U2</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/drake.jpg'); top: 4%; margin-left: 14%;"><span>Drake</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/taylor-swift.jpg'); top: auto; bottom: 0; margin-left: 23%;"><span>Taylor Swift</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/eminem.jpg'); top: auto; bottom: 36%; margin-left: 19%;"><span>Eminem</span></a>
                        <a href="#" class="singer-bb-image" style="background-image: url('demos/music/images/singers/sia.jpg'); top: auto; bottom: 0; margin-left: 12.5%;"><span>Sia</span></a>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="row topmargin-lg dark1 clearfix">
                    <!-- Soundcloud
                    ============================================= -->
                    <div class="col-lg-4 col-md-6">
                        <h3>Soundcloud Embed</h3>
                        <iframe width="100%" height="130" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/301161123&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe><br><br>
                        <iframe width="100%" height="130" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/278002889&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true"></iframe>
                    </div>

                    <!-- Mobile App
                    ============================================= -->
                    <div class="col-lg-7 col-md-5 offset-md-1 ohidden">
                        <div class="row clearfix">
                            <div class="col-lg-6 order-lg-12">
                                <div class="d-block d-md-none d-lg-none d-xl-block topmargin clear"></div>
                                <h3>Grab it Now in Mobile</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, impedit.</p>
                                <a href="#" class="button button-small button-rounded button-desc noleftmargin clearfix"><i class="icon-apple"></i><div>App Store<span>Free Forever, Downlod It Now</span></div></a>
                                <a href="#" class="button button-small button-rounded button-desc noleftmargin clearfix" style="margin-top: 10px"><i class="icon-android"></i><div>Google Play<span>Free Forever, Downlod It Now</span></div></a>
                            </div>
                            <div class="d-block d-md-block d-lg-none topmargin clear"></div>
                            <div class="col-lg-6 order-lg-1 d-sm-none d-md-none d-lg-block">
                                <img src="demos/music/images/iphone.png" alt="Mobile" data-animate="fadeInUp">
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- #content end -->



@endsection
